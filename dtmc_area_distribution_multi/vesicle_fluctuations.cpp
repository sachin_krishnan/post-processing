#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

int main(int argc, char *argv[])
{
	int f;
	int i;
  int j;
  int k;
  int in_time;	// Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;	// Final timestep (* in mem.*.vtp)	Eg. 500 fi_time >= in_time
  int st_time;	// Steps between consecutive timesteps Eg. 10
  int cu_time;	// Stores the current timestep
	int junk;	
  int num_st;
	int nbins;
	int OUT_FREQ;	// Output frequency in the MC code

	// Taking input through command line arguments
  if(argc < 7)
  {	
    cout<<"Usage : <exec> <number-of-bins> <initial_time> <final_time> <step_time> <output_frequency> <data-folder(s)"<<endl;
    return 0;
  }
	nbins = atoi(argv[1]);
  in_time = atoi(argv[2]);
  fi_time = atoi(argv[3]);
  st_time = atoi(argv[4]);
  OUT_FREQ = atoi(argv[5]);

  double progress;
  double vol;
	double sd;
	double max_vol = -1.0;
	double min_vol = 1e10;
  vector < double > vols;
	vector < int > vol_bins;
	string sjunk;
  char infile[256];
  string temp;
  string line;
  ifstream fp;
  ofstream fout;
  stringstream iss;
  
	//Finding mean radius from mean volume by processing memprops.dat
  k = 0;
  sd = 0.0;
	char fnames[256];

	for(f = 6;f < argc;f++)
	{
		sprintf(fnames, "%s/memprops.dat",argv[f]);
		fp.open(fnames, ios::in);
  	while(getline(fp,line))   
  	{
    	stringstream iss(line);
	    if(fp.eof())
				break;
    	if(line.c_str()[0] != '#')
			{
		  	getline(iss, sjunk, ' ');
			  cu_time = atoi(sjunk.c_str());
			  getline(iss, sjunk, ' ');
			  getline(iss, sjunk, ' ');
		 		getline(iss, sjunk, ' ');
			 	vol = atof(sjunk.c_str());
			  cu_time /= OUT_FREQ;
				
			  if(cu_time >= in_time && cu_time <= fi_time && (cu_time - in_time)%st_time == 0 && vol!=0.0)
		    {
				//	cout<<vol<<endl;
	    	  sd += vol;
					if(vol < min_vol)
						min_vol = vol;
					if(vol > max_vol)
						max_vol = vol;
					vols.push_back(vol);
		      k++;
		    }
			}
  	}
		fp.close();
	}
  sd /= (double) k;
  
  cout<<"Mean volume = "<<sd<<endl;
 	cout<<"Maximum volume = "<<max_vol<<endl;
	cout<<"Minimum volume = "<<min_vol<<endl;

	max_vol += 1.0;
	min_vol -= 1.0;
	double bin_width = (max_vol - min_vol) / nbins;

	for(k = 0;k < nbins;k++)
	{
		vol_bins.push_back(0);
	}
	
	for(k = 0;k < vols.size();k++)
	{
		vol_bins[(vols[k] - min_vol) / bin_width]++;
	}
	
	fout.open("aread.dat",ios::out);
	for(k = 0;k < nbins;k++)
	{
		fout<<min_vol + k*bin_width<<" "<<((double)vol_bins[k])/((double)vols.size())<<endl;
	}
	fout.close();
	
	// Clear everything
  cout<<"\nDone."<<endl;
	vols.clear();
	vol_bins.clear();
 	return 0;
}

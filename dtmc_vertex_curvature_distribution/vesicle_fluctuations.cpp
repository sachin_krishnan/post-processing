#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "extras.hpp"

using namespace std;

// We are re using the restart functions written in extras.cpp for the MC code here to read the output files and calculate per vertex properties. The variables below are the data structures needed to hold and process this information.
vector <Vert *> verts;
vector <Face *> faces;
vector <Edge *> edgef;
vector <Edge *> edgeb;
Vert *new_vertex;
Face *new_tri;
Edge *new_li;
Edge *new_mli;

int main(int argc, char *argv[])
{
	int f;
	int i;
  int j;
  int k;
	int l;
  int in_time;	// Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;	// Final timestep (* in mem.*.vtp)	Eg. 500 fi_time >= in_time
  int st_time;	// Steps between consecutive timesteps Eg. 10
  int cu_time;	// Stores the current timestep
  int nump;	// Number of vertices
	int numl;	// Number of edges
	int numt;	// Number of faces
	int junk;	
  int num_st;
  int nbins;

	// Taking input through command line arguments
  if(argc < 6)
  {	
    cout<<"Usage : <exec> <number-of-bins> <initial_time> <final_time> <step_time> <data-folder(s)"<<endl;
    return 0;
  }
	nbins = atoi(argv[1]);
  in_time = atoi(argv[2]);
  fi_time = atoi(argv[3]);
  st_time = atoi(argv[4]);

  double progress;
	double bin_width;
	string sjunk;
  char infile[256];
  string temp;
  string line;
  ifstream fp;
  ofstream fout;
  stringstream iss;
  double v21[3];
	double curv_max, curv_min;
	double d;
	char fnames[256];
	vector < double > curvs;
	vector < int > curv_dist;
 	num_st = 0;
	curv_min = 1000.0;
	curv_max = 0.0;

	for(i = 0 ;i < nbins;i++)
	{
		curv_dist.push_back(0);
	}

	for(f = 5;f < argc;f++)
	{
  	cu_time = in_time;
 	
 		// Processing the files one at a time
	  while(cu_time <= fi_time)
  	{
			// Prints the stylish progress bar
			cout<<"\rProcessing "<<argv[f]<<" "<<cu_time<<"...\t[";
		  progress = (double) (cu_time - in_time) * 20.0 / (double) (fi_time - in_time);
		  for(i = 1;i < 20;i++)
			{
			  if(i < progress)
			  	cout<<"#";
			  else
			  	cout<<" ";
			}
		  cout<<"]";
		  fflush(stdout);
		
			// Opens the configuration file and populates the data structures
			sprintf(infile, "%s/mem.%d.vtp", argv[f], cu_time);
			restartFromVTPFile(infile);
			nump = faces.size();
			for(i = 0;i < nump;i++)
			{
				faces[i]->calculateArea();
			}
			nump = verts.size();
		
		  for(i = 1;i < nump;i++)
			{
				verts[i]->calculateArea();
				verts[i]->calculateCurvature();
				d = verts[i]->mean_curvature;
				if(d > curv_max)
					curv_max = d;
				if(d < curv_min)
					curv_min = d;

				curvs.push_back(d);
	 		}
			
		 	verts.clear();
			faces.clear();
			edgef.clear();
			edgeb.clear();
		  cu_time += st_time;
  	  num_st++;
  	}	
		cout<<"\n";
	}

	cout<<"Maximum per vertex curvature = "<<curv_max<<endl;
	cout<<"Minimum per vertex curvature = "<<curv_min<<endl;
	curv_min -= 0.001;
	curv_max += 0.001;
	curv_min = -0.80;
	curv_max = 1.00;
	bin_width = (curv_max - curv_min) / nbins;

	for(i = 0;i < curvs.size();i++)
	{
	//	cout<<(bls[i]-bl_min)/bin_width<<endl;
		curv_dist[(int)((curvs[i] - curv_min) / bin_width)]++;
	}

	fout.open("vcurv_dist.dat", ios::out);
	for(j = 0;j < nbins;j++)
	{
		fout<<curv_min + bin_width*j<<"\t"<<((double)curv_dist[j])/((double)curvs.size())<<endl;
	}
	fout.close();

	
	// Clear everything
	curvs.clear();
	curv_dist.clear();
  cout<<"\nDone."<<endl;
 	return 0;
}

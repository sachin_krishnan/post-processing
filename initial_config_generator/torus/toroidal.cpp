#include<iostream>
#include<cmath>
#include<cstdlib>
#include<vector>
#include<fstream>

using namespace std;

int main(int argc, char *argv[])
{
	double bond_length;
	double theta_resolution;
	double phi_resolution;
	double major_radius;
	double minor_radius;
	double rows;
	double x, y, z;
	double theta;
	double phi;
	if(argc < 4)
	{
		cout<<"Usage : <exec> <theta_resolution> <phi_resolution> <bond_length>\n";
		return 0;
	}
	theta_resolution = atoi(argv[1]);
	phi_resolution = atoi(argv[2]);
	bond_length = atof(argv[3]);

	major_radius = phi_resolution * bond_length  / (2.0 * M_PI);
	minor_radius = theta_resolution * bond_length / (2.0 * M_PI);

	cout<<"Major radius = "<<major_radius<<endl;
	cout<<"Minor radius = "<<minor_radius<<endl;
	vector < vector <double> > r;
	vector < vector <int> > lines;
	vector < vector <int> > polys;
	int i, j, id, lid, tid;
	id = 0;
	lid = 0;
	tid = 0;
	for(i = 0;i < phi_resolution;i++)
	{
		for(j = 0;j < theta_resolution; j++)
		{
			r.push_back(vector <double> ());
//			theta = (j + ((i%2==0)?0.5:0.0)) * 2.0 * M_PI / (vertex_per_row);
			phi = i * 2.0 * M_PI / phi_resolution;
			theta = j * 2.0 * M_PI / theta_resolution;
//			cout<<phi<<" "<<theta<<" "<<endl;
			x = (major_radius + minor_radius * cos(theta)) * cos(phi);
			y = (major_radius + minor_radius * cos(theta)) * sin(phi);
			z = minor_radius * sin(theta);
			r[id].push_back(x);
			r[id].push_back(y);
			r[id].push_back(z);
//			cout<<phi<<" "<<theta<<" "<<x<<" "<<y<<" "<<z<<endl;
			lines.push_back(vector <int>());
			lines[lid].push_back(id);
			if(j != theta_resolution - 1)
			{
				lines[lid].push_back(id + 1);
			}
			else
			{
				lines[lid].push_back(i * theta_resolution);
			}
			lid++;
			lines.push_back(vector <int>());
			lines[lid].push_back(id);
			if(i != phi_resolution - 1)
			{
				lines[lid].push_back((i + 1) * theta_resolution + j);
			}
			else
			{
				lines[lid].push_back(j);
			}
			lid++;
			lines.push_back(vector <int>());
			lines[lid].push_back(id);
			if(i != phi_resolution - 1)
			{
				if(j != theta_resolution - 1)
				{
					lines[lid].push_back((i + 1) * theta_resolution + j + 1);
				}
				else
				{
					lines[lid].push_back((i + 1) * theta_resolution);
				}
			}
			else
			{
				if(j != theta_resolution - 1)
				{
					lines[lid].push_back(j + 1);
				}
				else
				{
					lines[lid].push_back(0);
				}
			}
			lid++;

			polys.push_back(vector <int>());
			polys[tid].push_back(id);
			if(i != phi_resolution - 1)
			{
				polys[tid].push_back((i + 1) * theta_resolution + j);
				if(j != theta_resolution - 1)
				{
					polys[tid].push_back((i + 1) * theta_resolution + j + 1);
				}
				else
				{
					polys[tid].push_back((i + 1) * theta_resolution);
				}
			}
			else
			{
				polys[tid].push_back(j);
				if(j != theta_resolution - 1)
				{
					polys[tid].push_back(j + 1);
				}
				else
				{
					polys[tid].push_back(0);
				}
			}
			tid++;
			polys.push_back(vector <int>());
			polys[tid].push_back(id);
			if(i != phi_resolution - 1)
			{
				if(j != theta_resolution - 1)
				{
					polys[tid].push_back((i + 1) * theta_resolution + j + 1);
					polys[tid].push_back(id + 1);
				}
				else
				{
					polys[tid].push_back((i + 1) * theta_resolution);
					polys[tid].push_back(i * theta_resolution);
				}
			}
			else
			{
				if(j != theta_resolution - 1)
				{
					polys[tid].push_back(j + 1);
					polys[tid].push_back(id + 1);
				}
				else
				{
					polys[tid].push_back(0);
					polys[tid].push_back(i * theta_resolution);
				}
			}
			tid++;
			id++;
		}
	}
	double rx, ry, rz;
	double d, min = 100, max = 0;
	for(i = 0;i < lines.size();i++)
	{
		rx = r[lines[i][0]][0] - r[lines[i][1]][0];
		ry = r[lines[i][0]][1] - r[lines[i][1]][1];
		rz = r[lines[i][0]][2] - r[lines[i][1]][2];
		d = sqrt(rx * rx + ry * ry + rz * rz);
		if(min > d)
			min = d;
		if(d > max)
			max = d;
		//cout<<i<<" "<<d<<endl;
	}
	cout<<"Minimum bond length = "<<min<<endl;
	cout<<"Maximum bond length = "<<max<<endl;
	ofstream outfile;
	outfile.open("torus.vtk", ios::out);
	outfile<<"# vtk DataFile Version 2.0\n";
	outfile<<"initial Output\n";
	outfile<<"ASCII\nDATASET POLYDATA\nPOINTS "<<id<<" float\n";
	for(i = 0;i < id;i++)
	{
		outfile<<r[i][0]<<" "<<r[i][1]<<" "<<r[i][2]<<endl;
	}
	outfile<<"LINES "<<lines.size()<<" "<<lines.size()*3<<endl;
	for(i = 0;i < lines.size();i++)
	{
		outfile<<"2 "<<lines[i][0]<<" "<<lines[i][1]<<endl;
	}
	outfile<<"POLYGONS "<<polys.size()<<" "<<polys.size()*4<<endl;
	for(i = 0;i < polys.size();i++)
	{
		outfile<<"3 "<<polys[i][0]<<" "<<polys[i][1]<<" "<<polys[i][2]<<endl;
	}
	cout<<"Number of vertices = "<<r.size()<<endl;
	cout<<"Number of edges = "<<lines.size()<<endl;
	cout<<"Number of faces = "<<polys.size()<<endl;
	outfile.close();
	r.clear();
	lines.clear();
	polys.clear();
	return 0;
}

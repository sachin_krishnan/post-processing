#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <vector>
#include <cmath>
using namespace std;

// Start from a restart file (legacy VTK Format)
int main(int argc, char *argv[])
{
  int i;
  int j;
  int k;
  int m;
  int nump;
  int numl;
  int numt;
  int id[12];
	int tid = 0;
  bool flag;
  bool done_flag;
  double rx;
  double ry;
  double rz;
	double d;
  string line;
  ifstream infile;
	ofstream outfile;
	vector < vector <double> > r;
	vector < vector <int> > edge_list;
	vector < vector <int> > triangles;
	vector < vector <int> > alias;
  infile.open(argv[1], ios::in);
	outfile.open(argv[2], ios::out);
  /* Reading the header */
  done_flag = false;
  while(!infile.eof() && !done_flag)
    {
      infile>>line;
      if(strcmp(line.c_str(),"POINTS")==0)
			{
	  		infile>>nump;
	  		infile>>line;		
				outfile<<"# vtk DataFile Version 3.0\n";
				outfile<<"vtk output\n";
				outfile<<"ASCII\n";
				outfile<<"DATASET POLYDATA\n";
				outfile<<"POINTS "<<nump<<" float\n";
	  		for(i = 0;i < nump;i++)
	    	{
	      	infile>>rx>>ry>>rz;
					r.push_back(vector <double>());
					r[i].push_back(rx);
					r[i].push_back(ry);
					r[i].push_back(rz);
//	      	outfile<<rx<<" "<<ry<<" "<<rz<<endl;
		//			d = sqrt(rx * rx + ry * ry + rz * rz);
//					cout<<d<<endl;
					
	    		edge_list.push_back(vector <int>());
					for(j = 0;j < nump;j++)
					{
						edge_list[i].push_back(0);
						alias.push_back(vector <int>());
					}
				}
				double min = 100.0;
				for(i = 0;i < nump;i++)
				{
					for(j = i+1;j < nump;j++)
					{
						rx = r[i][0] - r[j][0];
						ry = r[i][1] - r[j][1];
						rz = r[i][2] - r[j][2];
						d = sqrt(rx * rx + ry * ry + rz * rz);
						if(d < min && d != 0.0)
							min = d;
						if(d == 0.0)
						{
							alias[j].push_back(i);
						}
			//			cout<<i<<" "<<j<<" "<<d<<endl;
					}
				}
				double scale = 1.0 / min;
				for(i = 0;i < nump;i++)
				{
					r[i][0] *= scale;
					r[i][1] *= scale;
					r[i][2] *= scale;
					if(alias[i].size()==0)
						outfile<<r[i][0]<<" "<<r[i][1]<<" "<<r[i][2]<<endl;
				}
				/*for(i = 0;i < nump;i++)
				{
					cout<<i<<" ";
					for(j = 0 ;j < alias[i].size();j++)
					{
						cout<<alias[i][j]<<" ";	
					}
					cout<<endl;
				}*/
			}
      else if(strcmp(line.c_str(), "TRIANGLE_STRIPS")==0)
			{
	  		infile>>numt;
	  		infile>>line;
	  		for(i = 0;i < numt;i++)
	    	{
	      	infile>>line;
					for(j = 0;j < 12;j++)
					{
						infile>>id[j];
					}
					for(j = 0;j < 10;j++)
					{
						triangles.push_back(vector <int>());
						triangles[tid].push_back(id[j]);
						triangles[tid].push_back(id[j+1]);
						triangles[tid].push_back(id[j+2]);
						tid++;
						edge_list[id[j]][id[j+1]] = 1;
						edge_list[id[j+1]][id[j]] = 1;
						edge_list[id[j+1]][id[j+2]] = 1;
						edge_list[id[j+2]][id[j+1]] = 1;
						edge_list[id[j+2]][id[j]] = 1;
						edge_list[id[j]][id[j+2]] = 1;
					}
				}
				outfile<<"LINES "<<endl;
				for(i = 0;i < nump;i++)
				{
					for(j = i + 1;j < nump;j++)
					{
						if(edge_list[i][j] == 1)
							outfile<<"2 "<<i<<" "<<j<<endl;
					}
				}
				outfile<<"POLYGONS "<<triangles.size()<<" "<<4*triangles.size()<<endl;
				for(i = 0;i < triangles.size();i++)
				{
					outfile<<"3 "<<triangles[i][0]<<" "<<triangles[i][1]<<" "<<triangles[i][2]<<endl;
				}
				done_flag = true;
	    }	
  }	
  infile.close();
  outfile.close();
}


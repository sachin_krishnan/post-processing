#include<iostream>
#include<cmath>
#include<cstdlib>
#include<vector>
#include<fstream>

using namespace std;

int main(int argc, char *argv[])
{
	double bond_length;
	double vertex_per_row;
	double cylinder_radius;
	double cylinder_length;
	double rows;
	double x, y, z;
	double theta;

	if(argc < 4)
	{
		cout<<"Usage : <exec> <num_vertex_per_row> <num_rows> <bond_length>\n";
		return 0;
	}
	vertex_per_row = atoi(argv[1]);
	rows = atoi(argv[2]);
	bond_length = atof(argv[3]);

	cylinder_radius = vertex_per_row * bond_length / (2.0 * M_PI);
	cylinder_length = rows * bond_length * 0.5;

	cout<<"Radius of cylinder = "<<cylinder_radius<<endl;
	cout<<"Length of cylinder = "<<cylinder_length<<endl;
	vector < vector <double> > r;
	vector < vector <int> > lines;
	vector < vector <int> > polys;
	int i, j, id, lid, pid;
	id = 0;
	lid = 0;
	pid = 0;
	for(i = 0;i < rows;i++)
	{
		z = i * bond_length * 0.5;
		for(j = 0;j < vertex_per_row; j++)
		{
			r.push_back(vector <double> ());
			theta = (j + ((i%2==0)?0.5:0.0)) * 2.0 * M_PI / (vertex_per_row);
			x = cylinder_radius * cos(theta);
			y = cylinder_radius * sin(theta);
			r[id].push_back(x);
			r[id].push_back(y);
			r[id].push_back(z);

			lines.push_back(vector <int>());
			lines[lid].push_back(id);
			if(j != vertex_per_row - 1)
			{
				lines[lid].push_back(id + 1);
			}
			else
			{
				lines[lid].push_back(i * vertex_per_row);
			}
			lid++;
			if(i != rows - 1)
			{
				lines.push_back(vector <int>());
				lines[lid].push_back(id);
				if(i % 2 == 0)
				{
					if(j == vertex_per_row - 1)
					{
						lines[lid].push_back(id + 1);
					}
					else
					{
						lines[lid].push_back(id + vertex_per_row + 1);
					}
				}
				else
				{
					lines[lid].push_back(id + vertex_per_row);	
				}
				lid++;
				lines.push_back(vector <int>());
				lines[lid].push_back(id);
				if(i%2 == 1)
				{
					if(j == 0)
					{
						lines[lid].push_back((i + 1) * vertex_per_row - 1);
					}
					else
					{
						lines[lid].push_back(id + vertex_per_row - 1);
					}
				}
				else
				{
					lines[lid].push_back(id + vertex_per_row);
				}
				lid++;
			}

			if(i != rows - 1)
			{
				if(i % 2 == 0)
				{
					polys.push_back(vector <int> ());
					polys[pid].push_back(id);
					if(j != vertex_per_row - 1)
					{
						polys[pid].push_back(id + 1);
						polys[pid].push_back(id + vertex_per_row + 1);
					}
					else
					{
						polys[pid].push_back(i * vertex_per_row);
						polys[pid].push_back(id + 1);
					}
					pid++;
					polys.push_back(vector <int> ());
					polys[pid].push_back(id);
					if(j != vertex_per_row - 1)
					{
						polys[pid].push_back(id + vertex_per_row + 1);
						polys[pid].push_back(id + vertex_per_row);
					}
					else
					{
						polys[pid].push_back((i + 1) * vertex_per_row);
						polys[pid].push_back(id + vertex_per_row);
					}
					pid++;
				}
				else
				{
					polys.push_back(vector <int> ());
					polys[pid].push_back(id);
					if(j != vertex_per_row - 1)
					{
						polys[pid].push_back(id + 1);
						polys[pid].push_back(id + vertex_per_row);
					}
					else
					{
						polys[pid].push_back(i * vertex_per_row);
						polys[pid].push_back(id + vertex_per_row);
					}
					pid++;
					polys.push_back(vector <int> ());
					polys[pid].push_back(id);
					if(j != 0)
					{
						polys[pid].push_back(id + vertex_per_row);
						polys[pid].push_back(id + vertex_per_row - 1);
					}
					else
					{
						polys[pid].push_back(id + vertex_per_row);
						polys[pid].push_back((i + 2) * vertex_per_row - 1);
					}
					pid++;
				}
			}
	
			id++;
		}
	}

	ofstream outfile;
	outfile.open("initial.vtk", ios::out);
	outfile<<"# vtk DataFile Version 2.0\n";
	outfile<<"initial Output\n";
	outfile<<"ASCII\nDATASET POLYDATA\nPOINTS "<<id<<" float\n";
	for(i = 0;i < id;i++)
	{
		outfile<<r[i][0]<<" "<<r[i][1]<<" "<<r[i][2]<<endl;
	}
	outfile<<"LINES "<<lines.size()<<" "<<lines.size()*3<<endl;
	for(i = 0;i < lines.size();i++)
	{
		outfile<<"2 "<<lines[i][0]<<" "<<lines[i][1]<<endl;
	}
	outfile<<"POLYGONS "<<polys.size()<<" "<<polys.size()*4<<endl;
	for(i = 0;i < polys.size();i++)
	{
		outfile<<"3 "<<polys[i][0]<<" "<<polys[i][1]<<" "<<polys[i][2]<<endl;
	}
	cout<<"Number of vertices = "<<r.size()<<endl;
	cout<<"Number of edges = "<<lines.size()<<endl;
	cout<<"Number of faces = "<<polys.size()<<endl;
	outfile.close();
	r.clear();
	lines.clear();
	polys.clear();
	return 0;
}

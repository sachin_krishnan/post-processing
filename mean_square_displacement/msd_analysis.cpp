#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>

#define EPSILON 1e-8

using namespace std;

int main(int argc, char *argv[])
{
  long int i,im,lm,j;
  
  stringstream iss;
  double **r0;
	double **r;
  int nump;
  double rx, ry, rz; 
	int cu_time, in_time, fi_time;
  string line;	
	string temp;
	vector <double> msd;
  fstream infile;
  if(argc < 3)
	{
		cout<<"Usage: exec <initial_time> <final_time>\n";
		return 0;
	}
  in_time = atoi(argv[1]);
	fi_time = atoi(argv[2]);

	char fname[25];
  cu_time = in_time;
	
	sprintf(fname, "mem.0.vtp", cu_time);	
	infile.open(fname, ios::in); 
 	while(!infile.eof())
	{
		infile>>temp;
		if(strcmp(temp.c_str(),"<Piece") == 0)
		{
		  infile>>temp;
		  iss.str(temp);
		  getline(iss,line,'\"');
  		getline(iss,line,'\"');
			nump = atoi(line.c_str());
			r0 = (double **) malloc(sizeof(double *) * nump);
			for(i = 0;i < nump;i++)
				r0[i] = (double *) malloc(sizeof(double) * 3);
			r = (double **) malloc(sizeof(double *) * nump);
			for(i = 0;i < nump;i++)
				r[i] = (double *) malloc(sizeof(double) * 3);
		}
 		else if(strcmp(temp.c_str(),"<Points>")==0)
  	{
 			for(i = 0;i < 5;i++)
			  infile>>line;

			for(i = 0;i < nump;i++)
			{ 
			  infile>>rx>>ry>>rz;
		  	r0[i][0] = rx;
				r0[i][1] = ry;
				r0[i][2] = rz;
			}
		}
	}
	infile.close();


  while(cu_time <= fi_time)
  {
		sprintf(fname, "mem.%d.vtp", cu_time);	
  	infile.open(fname, ios::in); 
  	while(!infile.eof())
  	{
			infile>>temp;
  		if(strcmp(temp.c_str(),"<Points>")==0)
	  	{
  			for(i = 0;i < 5;i++)
				  infile>>line;
	
				for(i = 0;i < nump;i++)
				{ 
				  infile>>rx>>ry>>rz;
			  	r[i][0] = rx;
				  r[i][1] = ry;
				  r[i][2] = rz;
				}
			}
	  }
		infile.close();
		
		/* calculation of ensemble average of msd */
		double imsd = 0.0;
		for(i = 0;i < nump;i++)
		{
			imsd += sqrt(((r[i][0] - r0[i][0]) * (r[i][0] * r0[i][0])) + ((r[i][1] - r0[i][1]) * (r[i][1] - r0[i][1])) + ((r[i][2] - r0[i][2]) * (r[i][2] - r0[i][2])));
		}
		imsd /= nump;
		msd.push_back(imsd);
		cu_time++;
	}

	for(auto v : msd)
		cout<<v<<endl;

  for(i = 0;i < nump;i++)
  {
		if(r[i] != NULL)
	 		free(r[i]);
		if(r0[i] != NULL)
			free(r0[i]);
 	}
  if(r != NULL)
   	free(r);
  if(r0 != NULL)
		free(r0);

	return 0;
}

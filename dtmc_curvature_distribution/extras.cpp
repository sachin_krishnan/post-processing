
#include "extras.hpp"
#include "variables.hpp"
#include <fstream>
#include <vector>
#include <sstream>
#include <cstring>
#include <algorithm>

/* Generate the initial tetrahedron configuration - Creates vertices. Defines neighbors for each vertex. */
void genTetrahedron(int n, double b)
{
  int nt = 3 * n * (n + 1) * 0.5 + 1;
  int nt2 = 3 * n * (n - 1) * 0.5 + 1;
  int nver = 3 * n * n + 2;
  int i;
  int lz;
  int ll;
  int ls;

  int np;
  int in;
  int ite;
  int id;

  double dl;
  double theta;
  double H;

  for(i = 0;i < nver;i++)		
    {
      new_vertex = new Vert();
      verts.push_back(new_vertex);
    }

  dl = n * b;
  theta = asin(1.0 / sqrt(3.0));
  H = dl * cos(theta);
  id = 0;
  for(lz = 0;lz <= n;lz++)
    {
      ll = (lz == 0)?0:lz-1;
      for(ls = 0;ls <= ll;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * lz * sin(theta) * 0.5), (0.5 * ((n - lz) * b - dl) + ls * b), (H - b * lz * cos(theta)));
	}
    }

  for(lz = 1;lz <= n;lz++)
    {
      for(ls = 0;ls < lz;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * (0.5 * sqrt(3.0) * (lz - ls) - lz * sin(theta))), (0.5 * b * (lz -ls)), (H - b * lz * cos(theta)));
	}
    }

  for(lz = 1;lz <= n;lz++)
    {
      for(ls = 0;ls < lz;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * (0.5 * ls * sqrt(3.0) - lz * sin(theta))), (-0.5 * b * ls), (H - b * lz * cos(theta)));
	}
    }

  verts[0]->num_neighbors = 3;

  for(i = 0;i < 3;i++)
    {
      verts[0]->vertex[i] = verts[i * n * (n + 1) * 0.5 + 1];
		
      lz = 1;
      ls = 0;
      np = (i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 1;
      verts[np]->num_neighbors = 6;
      in = (i + 1) % 3;

      verts[np]->vertex[0] = verts[in * n * (n + 1) * 0.5 + 1];
      verts[np]->vertex[1] = verts[0];

      in = (i + 2) % 3;

      verts[np]->vertex[2] = verts[in * n * (n + 1) * 0.5 + 1];
      verts[np]->vertex[3] = verts[in * n * (n + 1) * 0.5 + 3];
      verts[np]->vertex[4] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + 1];
      verts[np]->vertex[5] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + ls + 2];

      for(lz = 2;lz <= n;lz++)
	{
	  for(ls = 0;ls < lz;ls++)
	    {
	      np = (i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 1;

	      verts[np]->num_neighbors = 6;
	      if(ls != 0 && lz != n)
		{
		  if(ls != lz - 1)
		    {
		      verts[np]->vertex[0] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 2];
		      verts[np]->vertex[1] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + 1];
		    }
		  else 
		    {
		      ite = ((i + 1) * n * (n + 1) + lz * (lz - 1)) * 0.5 + 2;
		      if(ite > nt)
			ite = ite - nt + 1;
		      verts[np]->vertex[0] = verts[ite - 1];
		      ite = ((i + 1) * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + 2;
		      if(ite > nt)
			ite = ite - nt + 1;
		      verts[np]->vertex[1] = verts[ite - 1];
		    }
					
		  verts[np]->vertex[2] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls];
		  verts[np]->vertex[3] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls];
		  verts[np]->vertex[4] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + ls + 1];
		  verts[np]->vertex[5] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + ls + 2];
		}
	      else if(ls == 0 && lz != n)
		{
		  in = (i + 2) % 3;
		  verts[np]->vertex[0] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 2];
		  verts[np]->vertex[1] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + 1];
		  verts[np]->vertex[2] = verts[(in * n * (n + 1) + lz * (lz - 1)) * 0.5 + lz];
		  verts[np]->vertex[3] = verts[(in * n * (n + 1) + lz * (lz + 1)) * 0.5 + lz + 1];
		  verts[np]->vertex[4] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + ls + 1];
		  verts[np]->vertex[5] = verts[(i * n * (n + 1) + lz * (lz + 1)) * 0.5 + ls + 2];
		}
	      else if(ls != 0 && lz == n)
		{
		  if(ls != lz - 1)
		    {
		      verts[np]->vertex[0] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 2];
		      verts[np]->vertex[1] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + 1];
		      verts[np]->vertex[5] = verts[(i * n * (n - 1) + (n - 1) * (n - 2)) * 0.5 + ls + nt + 1];
		    }
		  else
		    {
		      ite = ((i + 1) * n * (n + 1) + lz * (lz - 1)) * 0.5 + 2;
		      if(ite > nt)
			ite = ite - nt + 1;
		      verts[np]->vertex[0] = verts[ite - 1];
		      ite = ((i + 1) * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + 2;
		      if(ite > nt)
			ite = ite - nt + 1;
		      verts[np]->vertex[1] = verts[ite - 1];
		      in = (i + 1) % 3;
		      verts[np]->vertex[5] = verts[(in * n * (n - 1) + (n - 1) * (n - 2)) * 0.5 + nt + 1];
		    }

		  verts[np]->vertex[2] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls];
		  verts[np]->vertex[3] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls];
		  verts[np]->vertex[4] = verts[(i * n * (n - 1) + (n - 1) * (n - 2)) * 0.5 + ls + nt];
		}else if(ls == 0 && lz == n)
		{
		  in = (i + 2) % 3;
		  verts[np]->vertex[0] = verts[(i * n * (n + 1) + lz * (lz - 1)) * 0.5 + ls + 2];
		  verts[np]->vertex[1] = verts[(i * n * (n + 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + 1];
		  verts[np]->vertex[2] = verts[(in * n * (n + 1) + lz * (lz - 1)) * 0.5 + lz];
		  verts[np]->vertex[3] = verts[(i * n * (n - 1) + (n - 1) * (n - 2)) * 0.5 + nt + 1];
		  verts[np]->num_neighbors = 4;
		}
	    }
	}
    }
  for(lz = 0;lz < n;lz++)
    {
      ll = (lz == 0)?0:(lz - 1);
      for(ls = 0;ls <= ll;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * lz * sin(theta) * 0.5), (0.5 * ((n - lz) * b - dl) + ls * b), (- H + b * lz * cos(theta)));
	}
    }

  for(lz = 1;lz < n;lz++)
    {
      for(ls = 0;ls < lz;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * (0.5 * sqrt(3.0) * (lz - ls) - lz * sin(theta))), (0.5 * b * (lz -ls)), (- H + b * lz * cos(theta)));
	}
    }

  for(lz = 1;lz < n;lz++)
    {
      for(ls = 0;ls < lz;ls++)
	{
	  new_vertex = verts[id];
	  new_vertex->id = id++;
	  new_vertex->setCoord((b * (0.5 * ls * sqrt(3.0) - lz * sin(theta))), (-0.5 * b * ls), (- H + b * lz * cos(theta)));
	}
    }

  verts[nt]->num_neighbors = 3;

  for(i = 0;i < 3;i++)
    {
      verts[nt]->vertex[2-i] = verts[(i * n * (n - 1)) * 0.5 + 1 + nt];
      lz = 1;
      ls = 0;
      np = (i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt + 1;
      verts[np]->num_neighbors = 6;
		
      in = (i + 1) % 3;

      verts[np]->vertex[5] = verts[(in * n * (n - 1)) * 0.5 + nt + 1];
      verts[np]->vertex[4] = verts[nt];

      in = (i + 2) % 3;
      verts[np]->vertex[3] = verts[(in * n * (n - 1)) * 0.5 + nt + 1];
      verts[np]->vertex[2] = verts[(in * n * (n - 1)) * 0.5 + nt + 3];
      verts[np]->vertex[1] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + nt + 1];
      verts[np]->vertex[0] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + ls + nt + 2];

      for(lz = 2;lz < n;lz++)
	{
	  for(ls = 0;ls < lz;ls++)
	    {
	      np = (i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt + 1;
	      verts[np]->num_neighbors = 6;
	      if(ls != 0 && lz != (n - 1))
		{
		  if(ls != lz - 1)
		    {
		      verts[np]->vertex[5] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt + 2];
		      verts[np]->vertex[4] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt + 1];
		    }
		  else
		    {
		      ite = ((i + 1) * n * (n - 1) + lz * (lz - 1)) * 0.5 + 2;
		      if(ite > nt2)
			ite = ite - nt2 + 1;
		      verts[np]->vertex[5] = verts[ite + nt - 1];
		      ite = ((i + 1) * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + 2;
		      if(ite > nt2)
			ite = ite - nt2 + 1;
		      verts[np]->vertex[4] = verts[ite + nt - 1];
		    }
		  verts[np]->vertex[3] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt];
		  verts[np]->vertex[2] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt];
		  verts[np]->vertex[1] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + ls + nt + 1];
		  verts[np]->vertex[0] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + ls + nt + 2];
		}
	      else if(ls == 0 && lz != (n - 1))
		{
		  in = (i + 2) % 3;
		  verts[np]->vertex[5] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt + 2];
		  verts[np]->vertex[4] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt + 1];
		  verts[np]->vertex[3] = verts[(in * n * (n - 1) + lz * (lz - 1)) * 0.5 + lz + nt];
		  verts[np]->vertex[2] = verts[(in * n * (n - 1) + lz * (lz + 1)) * 0.5 + lz + nt + 1];
		  verts[np]->vertex[1] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + ls + nt + 1];
		  verts[np]->vertex[0] = verts[(i * n * (n - 1) + lz * (lz + 1)) * 0.5 + ls + nt + 2];
		}
	      else if(ls != 0 && lz == n - 1)
		{
		  if(ls != lz - 1)
		    {
		      verts[np]->vertex[5] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 +ls + nt + 2];
		      verts[np]->vertex[4] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt + 1];
		    }
		  else
		    {
		      ite = ((i + 1) * n * (n - 1) + lz * (lz - 1)) * 0.5 + 2;
		      if(ite > nt2)
			ite = ite - nt2 + 1;
		      verts[np]->vertex[5] = verts[ite + nt - 1];
		      ite = ((i + 1) * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + 2;
		      if(ite > nt2)
			ite = ite - nt2 + 1;
		      verts[np]->vertex[4] = verts[ite + nt - 1];
		    }
		  verts[np]->vertex[3] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt];
		  verts[np]->vertex[2] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt];
		  verts[np]->vertex[1] = verts[(i * n * (n + 1) + n * (n - 1)) * 0.5 + ls + 1];
		  verts[np]->vertex[0] = verts[(i * n * (n + 1) + n * (n - 1)) * 0.5 + ls + 2];
		}
	      else if(ls == 0 && lz == (n - 1))
		{
		  in = (i + 2) % 3;
		  verts[np]->vertex[5] = verts[(i * n * (n - 1) + lz * (lz - 1)) * 0.5 + ls + nt + 2];
		  verts[np]->vertex[4] = verts[(i * n * (n - 1) + (lz - 1) * (lz - 2)) * 0.5 + ls + nt + 1];
		  verts[np]->vertex[3] = verts[(in * n * (n - 1) + lz * (lz - 1)) * 0.5 + lz + nt];
		  verts[np]->vertex[2] = verts[(in * n * (n + 1) + n * (n - 1)) * 0.5 + n];
		  verts[np]->vertex[1] = verts[(i * n * (n + 1) + n * (n - 1)) * 0.5 + 1];
		  verts[np]->vertex[0] = verts[(i * n * (n + 1) + n * (n - 1)) * 0.5 + 2];
		}
	    }
	}
    }
}

// Start from a restart file (legacy VTK Format)
void restartFromVTKFile(char *fileName)
{
  int i;
  int j;
  int k;
  int k1;
  int kp1;
  int km1;
  int l;
  int l1;
  int lp1;
  int m;
  int ord_count;
  int nump;
  int numl;
  int numt;
  int id1;
  int id2;
  int id3;
  bool flag;
  bool done_flag;
  double rx;
  double ry;
  double rz;
  string line;
  ifstream infile;
  bool true_neigh;
  bool *ordchk;
  Vert *ver_tmp;
  Edge *li_tmp;

  infile.open(fileName, ios::in);
  /* Reading the header */
  done_flag = false;
  while(!infile.eof() && !done_flag)
    {
      infile>>line;
      if(strcmp(line.c_str(),"POINTS")==0)
	{
	  infile>>nump;
	  infile>>line;		
	  for(i = 0;i < nump;i++)
	    {
	      infile>>rx>>ry>>rz;
	      
	      new_vertex = new Vert();
	      verts.push_back(new_vertex);
	      new_vertex->id = i;
	      new_vertex->setCoord(rx,ry,rz);
	    }
	}
      else if(strcmp(line.c_str(),"LINES")==0)
	{
	  infile>>numl;
	  infile>>line;		
	  new_li = new Edge();
	  new_mli = new Edge();
	  edgef.push_back(new_li);
	  edgeb.push_back(new_mli);
	  for(i = 0;i < numl;i++)
	    {
	      infile>>line>>id1>>id2;
	      new_li = new Edge();
	      new_mli = new Edge();
	      new_li->vertex[0] = verts[id1];
	      new_li->vertex[1] = verts[id2];
	      new_li->opposite = new_mli;
	      new_mli->vertex[0] = verts[id2];
	      new_mli->vertex[1] = verts[id1];
	      new_mli->opposite = new_li;
	      new_li->lengthCheck();
	      new_mli->lengthCheck();
	      edgef.push_back(new_li);
	      edgeb.push_back(new_mli);
	      
	      flag = false;
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
		{
		  if(verts[id1]->vertex[j]->id == id2)
		    flag = true;
		}
	      if(!flag)
		{
		  verts[id1]->vertex[verts[id1]->num_neighbors] = verts[id2];
		  verts[id1]->edge[verts[id1]->num_neighbors] = new_li;
		  verts[id1]->num_neighbors++;
		  verts[id2]->vertex[verts[id2]->num_neighbors] = verts[id1];
		  verts[id2]->edge[verts[id2]->num_neighbors] = new_mli;
		  verts[id2]->num_neighbors++;
		}
	    }
	}
      else if(strcmp(line.c_str(), "POLYGONS")==0)
	{
	  infile>>numt;
	  infile>>line;
	  for(i = 0;i < numt;i++)
	    {
	      infile>>line>>id1>>id2>>id3;
	      new_tri = new Face();
	      new_tri->vertex[0] = verts[id1];
	      new_tri->vertex[1] = verts[id2];
	      new_tri->vertex[2] = verts[id3];
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
		{
		  if(verts[id1]->vertex[j]->id == id2)
		    {
		      new_tri->edge[0] = verts[id1]->edge[j];
		      new_tri->edge[0]->face = new_tri;
		    }
		}
	      for(j = 0;j < verts[id2]->num_neighbors;j++)
		{
		  if(verts[id2]->vertex[j]->id == id3)
		    {
		      new_tri->edge[1] = verts[id2]->edge[j];
		      new_tri->edge[1]->face = new_tri;
		    }
		}
	      for(j = 0;j < verts[id3]->num_neighbors;j++)
		{
		  if(verts[id3]->vertex[j]->id == id1)
		    {
		      new_tri->edge[2] = verts[id3]->edge[j];
		      new_tri->edge[2]->face = new_tri;
		    }
		}
	     faces.push_back(new_tri);
	    }
	  done_flag = true;
	}
    }	
  infile.close();
  restartOrdering();
}

// Start from a restart file (VTP format) 
void restartFromVTPFile(char *fileName)
{
  int i;
  int j;
  int k;
  int k1;
  int kp1;
  int km1;
  int l;
  int l1;
  int lp1;
  int m;
  int ord_count;
  int nump = 0;
  int numl = 0;
  int numt = 0;
  int id1;
  int id2;
  int id3;
	int phi;
  bool flag;
  bool done_flag;
  double rx;
  double ry;
  double rz;
  string line;
  string temp;
  ifstream infile;
  stringstream iss;
  bool true_neigh;
  bool *ordchk;
  Vert *ver_tmp;
  Edge *li_tmp;
  
  infile.open(fileName, ios::in);

  /* Reading the header */
  done_flag = false;
  while(!infile.eof() && !done_flag)
  {
    infile>>line;
    if(strcmp(line.c_str(),"<Piece")==0)
		{
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
	  	getline(iss,temp,'\"');
		  nump = atoi(temp.c_str());
		  infile>>line;
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
	  	getline(iss,temp,'\"');
		  numl = atoi(temp.c_str());
		  infile>>line;
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
		  getline(iss,temp,'\"');
		  numt = atoi(temp.c_str());
		}
    else if(strcmp(line.c_str(),"<Points>")==0)
		{
	  	for(i = 0;i < 5;i++)
	    	infile>>line;
		  for(i = 0;i < nump;i++)
	    {
	      infile>>rx>>ry>>rz;
	      new_vertex = new Vert();
	      verts.push_back(new_vertex);
	      new_vertex->id = i;
	      new_vertex->setCoord(rx,ry,rz);
	    }
		}
    else if(strcmp(line.c_str(),"<Lines>")==0)
		{
	  	for(i = 0;i < 4;i++)
	    	infile>>line;
		  new_li = new Edge();
		  new_mli = new Edge();
	  	edgef.push_back(new_li);
		  edgeb.push_back(new_mli);
		  for(i = 0;i < numl;i++)
	  	{
	      infile>>id1>>id2;
	      new_li = new Edge();
	      new_mli = new Edge();
	      new_li->vertex[0] = verts[id1];
	      new_li->vertex[1] = verts[id2];
	      new_li->opposite = new_mli;
	      new_mli->vertex[0] = verts[id2];
	      new_mli->vertex[1] = verts[id1];
	      new_mli->opposite = new_li;
	      new_li->lengthCheck();
	      new_mli->lengthCheck();
	      edgef.push_back(new_li);
	      edgeb.push_back(new_mli);

	      flag = false;
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
				{
				  if(verts[id1]->vertex[j]->id == id2)
		  		  flag = true;
				}
	      if(!flag)
				{
				  verts[id1]->vertex[verts[id1]->num_neighbors] = verts[id2];
				  verts[id1]->edge[verts[id1]->num_neighbors] = new_li;
				  verts[id1]->num_neighbors++;
				  verts[id2]->vertex[verts[id2]->num_neighbors] = verts[id1];
				  verts[id2]->edge[verts[id2]->num_neighbors] = new_mli;
				  verts[id2]->num_neighbors++;
				}
	    }
    }
    else if(strcmp(line.c_str(), "<Polys>")==0)
		{
		  for(i = 0;i < 4;i++)
	  	  infile>>line;
		  for(i = 0;i < numt;i++)
	    {
	      infile>>id1>>id2>>id3;
	      new_tri = new Face();
	      new_tri->vertex[0] = verts[id1];
	      new_tri->vertex[1] = verts[id2];
	      new_tri->vertex[2] = verts[id3];
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
				{
		  		if(verts[id1]->vertex[j]->id == id2)
			    {
			      new_tri->edge[0] = verts[id1]->edge[j];
			      new_tri->edge[0]->face = new_tri;
		  	  }
				}
	      for(j = 0;j < verts[id2]->num_neighbors;j++)
				{
				  if(verts[id2]->vertex[j]->id == id3)
			    {
		  	    new_tri->edge[1] = verts[id2]->edge[j];
		    	  new_tri->edge[1]->face = new_tri;
			    }
				}
	      for(j = 0;j < verts[id3]->num_neighbors;j++)
				{
				  if(verts[id3]->vertex[j]->id == id1)
		    	{
			      new_tri->edge[2] = verts[id3]->edge[j];
			      new_tri->edge[2]->face = new_tri;
			    }
				}
	      faces.push_back(new_tri);
	    }
	  //	done_flag = true;
		}
		else if(strcmp(line.c_str(), "<PointData")==0)
		{
			for(i = 0;i < 5;i++)
				infile>>line;
			for(i = 0;i < nump;i++)
			{
				infile>>phi;
				verts[i]->phi = phi;
			}
			done_flag = true;
		}
  }
	
  infile.close();
  restartOrdering();
}


/* Ordering after restarting from file */
void restartOrdering()
{
  int i;
  int j;
  int k;
  int jp1;
  int num_neigh;
  int num_neigh_neigh;
  int ver_id;
  int next_ver_id;
  int correct_next_ver_id;
  int next_ver_loc;
  Vert *tmpvert;
  Edge *tmpedge;
    // Sorting of vertices - 02 September 2015
  for(i = 0;i < verts.size();i++)
    {
      num_neigh = verts[i]->num_neighbors;
      for(j = 0;j < num_neigh;j++)
	{
	  ver_id = verts[i]->vertex[j]->id;
	  jp1 = (j + 1) % num_neigh;
	  next_ver_id = verts[i]->vertex[jp1]->id;
	  for(k = 0;k < 3;k++)
	    {
	      if(verts[i]->edge[j]->face->vertex[k]->id != i && verts[i]->edge[j]->face->vertex[k]->id != ver_id)
		{
		  correct_next_ver_id = verts[i]->edge[j]->face->vertex[k]->id;
		}
	    }
	  if(next_ver_id != correct_next_ver_id)
	    {
	      for(k = 0;k < num_neigh;k++)
		{
		  if(verts[i]->vertex[k]->id == correct_next_ver_id)
		    next_ver_loc = k;
		}
	      tmpvert = verts[i]->vertex[jp1];
	      tmpedge = verts[i]->edge[jp1];
	      verts[i]->vertex[jp1] = verts[i]->vertex[next_ver_loc];
	      verts[i]->edge[jp1] = verts[i]->edge[next_ver_loc];
	      verts[i]->vertex[next_ver_loc] = tmpvert;
	      verts[i]->edge[next_ver_loc] = tmpedge;
	    }
	}
    }
}



/* Creates links and triangles from the vertex and neighbor information */
void ordering()
{
  int i;
  int j;
  int kn;
  int ikn;
  int knp1;
  int n1;
  int n2;
  int nt;
  int nlin;
  int trno;
  int nver;
  int **trl;
  int **tl;

  tl = 0;
  nt = -1;
  nlin = 0;

  new_li = new Edge();
  new_mli = new Edge();
  edgef.push_back(new_li); 				// a blank element
  edgeb.push_back(new_mli);				// a blank element
  nver = verts.size();

  trl = (int **)malloc(sizeof(int *) * nver * nver);
  for(i = 0;i < nver;i++)
    trl[i] = (int *)malloc(sizeof(int) * nver);
	
  tl = (int **)malloc(sizeof(int *) * nver * nver);
  for(i = 0;i < nver;i++)
    tl[i] = (int *)malloc(sizeof(int) * nver);

  for(i = 0;i < nver;i++)
    {
      for(j = 0;j < nver;j++)
	{
	  trl[i][j] = -1;
	  tl[i][j] = 0;
	}
    }

  for(kn = 0;kn < nver;kn++)
    {
      for(ikn = 0;ikn < verts[kn]->num_neighbors;ikn++)
	{
	  knp1 = (ikn + 1) % verts[kn]->num_neighbors;
	  n1 = verts[kn]->vertex[ikn]->id;
	  n2 = verts[kn]->vertex[knp1]->id;

	  if(trl[kn][n1] == -1)
	    {
	      nt++;
	      new_tri = new Face();
	      faces.push_back(new_tri);
	      faces[nt]->vertex[0] = verts[kn];
	      faces[nt]->vertex[1] = verts[n1];
	      faces[nt]->vertex[2] = verts[n2];
	      trl[kn][n1] = nt;
	      trl[n1][n2] = nt;
	      trl[n2][kn] = nt;

	      if(tl[kn][n1] == 0)
		{
		  nlin++;
		  new_li = new Edge();
		  new_mli = new Edge();
		  edgef.push_back(new_li);
		  edgeb.push_back(new_mli);
		  edgef[nlin]->vertex[0] = verts[kn];
		  edgef[nlin]->vertex[1] = verts[n1];
		  edgeb[nlin]->vertex[0] = verts[n1];
		  edgeb[nlin]->vertex[1] = verts[kn];
		  edgef[nlin]->opposite = edgeb[nlin];
		  edgeb[nlin]->opposite = edgef[nlin];
		  edgef[nlin]->lengthCheck();
		  edgeb[nlin]->lengthCheck();
		  tl[kn][n1] = nlin;
		  tl[n1][kn] = -nlin;
		}

	      if(tl[n1][n2] == 0)
		{
		  nlin++;
		  new_li = new Edge();
		  new_mli = new Edge();
		  edgef.push_back(new_li);
		  edgeb.push_back(new_mli);
		  edgef[nlin]->vertex[0] = verts[n1];
		  edgef[nlin]->vertex[1] = verts[n2];
		  edgeb[nlin]->vertex[0] = verts[n2];
		  edgeb[nlin]->vertex[1] = verts[n1];
		  edgef[nlin]->opposite = edgeb[nlin];
		  edgeb[nlin]->opposite = edgef[nlin];
		  edgef[nlin]->lengthCheck();
		  edgeb[nlin]->lengthCheck();
		  tl[n1][n2] = nlin;
		  tl[n2][n1] = -nlin;
		}

	      if(tl[n2][kn] == 0)
		{
		  nlin++;
		  new_li = new Edge();
		  new_mli = new Edge();
		  edgef.push_back(new_li);
		  edgeb.push_back(new_mli);
		  edgef[nlin]->vertex[0] = verts[n2];
		  edgef[nlin]->vertex[1] = verts[kn];
		  edgeb[nlin]->vertex[0] = verts[kn];
		  edgeb[nlin]->vertex[1] = verts[n2];
		  edgef[nlin]->opposite = edgeb[nlin];
		  edgeb[nlin]->opposite = edgef[nlin];
		  edgef[nlin]->lengthCheck();
		  edgeb[nlin]->lengthCheck();		  
		  tl[n2][kn] = nlin;
		  tl[kn][n2] = -nlin;
		}

	      if(tl[kn][n1] > 0)
		{
		  edgef[tl[kn][n1]]->face = faces[nt];
		  faces[nt]->edge[0] = edgef[tl[kn][n1]];
		}
	      else
		{
		  edgeb[tl[n1][kn]]->face = faces[nt];
		  faces[nt]->edge[0] = edgeb[-tl[kn][n1]];
		}
	      if(tl[n1][n2] > 0)
		{
		  edgef[tl[n1][n2]]->face = faces[nt];
		  faces[nt]->edge[1] = edgef[tl[n1][n2]];
		}
	      else
		{
		  edgeb[tl[n2][n1]]->face = faces[nt];
		  faces[nt]->edge[1] = edgeb[-tl[n1][n2]];
		}

	      if(tl[n2][kn] > 0)
		{
		  edgef[tl[n2][kn]]->face = faces[nt];
		  faces[nt]->edge[2] = edgef[tl[n2][kn]];
		}
	      else
		{
		  edgeb[tl[kn][n2]]->face = faces[nt];
		  faces[nt]->edge[2] = edgeb[-tl[n2][kn]];
		}
	    }
	  else
	    {
	      trno = trl[kn][n1];

	      if(tl[kn][n1] > 0)
		edgef[tl[kn][n1]]->face = faces[trno];
	      else
		edgeb[tl[n1][kn]]->face = faces[trno];

	      if(tl[n1][n2] > 0)
		edgef[tl[n1][n2]]->face = faces[trno];
	      else
		edgeb[tl[n2][n1]]->face = faces[trno];

	      if(tl[n2][kn] > 0)
		edgef[tl[n2][kn]]->face = faces[trno];
	      else
		edgeb[tl[kn][n2]]->face = faces[trno];
	    }
	}
    }

  for(i = 0;i < nver;i++)
    {
      for(j = 0;j < verts[i]->num_neighbors;j++)
	{
	  n1 = verts[i]->vertex[j]->id;
	  if(tl[i][n1] > 0)
	    verts[i]->edge[j] = edgef[tl[i][n1]];
	  else
	    verts[i]->edge[j] = edgeb[tl[n1][i]];
	}
    }

  for(i = 0;i < nver;i++)
    {
      if(trl[i] != NULL)
	free(trl[i]);
      if(tl[i] != NULL)
	free(tl[i]);
    }

  if(trl != NULL)
    free(trl);
  if(tl != NULL)
    free(tl);
}

/* Bond length checking for maintaining self-avoidance */
int lengthCheck(Vert *ver1, Vert *ver2)							// returns - (1 if bl <= 1.0) (0 otherwise)
{
  double dis;
  double v21[3];
  v21[0] =  ver1->coord[0] - ver2->coord[0];
  v21[1] =  ver1->coord[1] - ver2->coord[1];
  v21[2] =  ver1->coord[2] - ver2->coord[2];
  dis = v21[0] * v21[0] + v21[1] * v21[1] + v21[2] * v21[2];
  if(dis <= 1.0) 
    return 1;
  else
    return 0;
}


/*
void buildCellList()
{
  int i = 0, j;
  int p, q, r;
  int x, y, z;
  int nverts = verts.size();
  int nfaces = faces.size();

  for(Face *f0 : faces)
    {
      f0->findCell();
    }

  for(Vert *ve : verts)
    {
      ve->findCell();
      cellList[i].clear();
      neighList[i++].clear();
    }

  for(i = 0;i < nverts;i++)
    {
      p = verts[i]->cell[0];
      q = verts[i]->cell[1];
      r = verts[i]->cell[2];
      for(j = i + 1; j < nverts;j++)
	{
	  x = verts[j]->cell[0];
	  y = verts[j]->cell[1];
	  z = verts[j]->cell[2];
	  if(abs(x - p) <= 1)
	    {
	      if(abs(y - q) <= 1)
		{
		  if(abs(z - r) <= 1)
		    {
		      cellList[i].push_back(j);
		      cellList[j].push_back(i);
		    }
		}
	    }
	}

      for(j = 0;j < nfaces;j++)
	{
	  x = faces[j]->cell[0];
	  y = faces[j]->cell[1];
	  z = faces[j]->cell[2];
	  if(abs(x - p) <= 1)
	    {
	      if(abs(y - q) <= 1)
		{
		  if(abs(z - r) <= 1)
		    {
		      neighList[i].push_back(j);
		    }
		}
	    }
	}
    }
}
*/

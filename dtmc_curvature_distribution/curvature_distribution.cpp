#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "extras.hpp"

using namespace std;

// We are re using the restart functions written in extras.cpp for the MC code here to read the output files and calculate per vertex properties. The variables below are the data structures needed to hold and process this information.
vector <Vert *> verts;
vector <Face *> faces;
vector <Edge *> edgef;
vector <Edge *> edgeb;
Vert *new_vertex;
Face *new_tri;
Edge *new_li;
Edge *new_mli;

int main(int argc, char *argv[])
{
  int f;
  int i;
  int j;
  int k;
  int l;
  int in_time;	// Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;	// Final timestep (* in mem.*.vtp)	Eg. 500 fi_time >= in_time
  int st_time;	// Steps between consecutive timesteps Eg. 10
  int cu_time;	// Stores the current timestep
  int nump;	// Number of vertices
  int numl;	// Number of edges
  int numt;	// Number of faces
  int junk;	
  int num_st;
  int nbins;

  // Taking input through command line arguments
  if(argc < 6)
    {	
      cout<<"Usage : <exec> <number-of-bins> <initial_time> <final_time> <step_time> <data-folder(s)"<<endl;
      return 0;
    }
  nbins = atoi(argv[1]);
  in_time = atoi(argv[2]);
  fi_time = atoi(argv[3]);
  st_time = atoi(argv[4]);

  double progress;
  double bin_width1, bin_width2;
  string sjunk;
  char infile[256];
  char outfile[256];
  string temp;
  string line;
  ifstream fp;
  ofstream fout;
  stringstream iss;
  double v21[3];
  double mcurv_max, mcurv_min;
  double gcurv_max, gcurv_min;
  double d;
  char fnames[256];
  vector < vector < double > > curvs;
  vector < vector < int > > curv_dist;
  num_st = 0;


  for(f = 5;f < argc;f++)
    {
      cu_time = in_time;
 	
      // Processing the files one at a time
      while(cu_time <= fi_time)
  	{
	  // Prints the stylish progress bar
	  cout<<"\rProcessing "<<argv[f]<<" "<<cu_time<<"...\t[";
	  progress = (double) (cu_time - in_time) * 20.0 / (double) (fi_time - in_time);
	  for(i = 1;i < 20;i++)
	    {
	      if(i < progress)
		cout<<"#";
	      else
		cout<<" ";
	    }
	  cout<<"]";
	  fflush(stdout);
	  for(i = 0 ;i <= nbins;i++)
	    {
	      vector <int> tcurv_dist;
	      for(j = 0;j <= nbins;j++)
		tcurv_dist.push_back(0);
	      curv_dist.push_back(tcurv_dist);
	    }
       	  // mcurv_min = 100.0;
	  // mcurv_max = -2.0;
	  // gcurv_min = 100.0;
	  // gcurv_max = -2.0;

	  // Opens the configuration file and populates the data structures
	  sprintf(infile, "%s/mem.%d.vtp", argv[f], cu_time);
	  restartFromVTPFile(infile);
	  nump = faces.size();
	  for(i = 0;i < nump;i++)
	    {
	      faces[i]->calculateArea();
	    }
	  nump = verts.size();
		
	  for(i = 0;i < nump;i++)
	    {
	      vector <double> tcurvs;
	      verts[i]->calculateArea();
	      verts[i]->calculateCurvature();
	      d = verts[i]->mean_curvature;
	      // if(d > mcurv_max)
	      // 	mcurv_max = d;
	      // if(d < mcurv_min)
	      // 	mcurv_min = d;
        
	      tcurvs.push_back(d);

	      d = verts[i]->gaussian_curvature;
	      // if(d > gcurv_max)
	      // 	gcurv_max = d;
	      // if(d < gcurv_min)
	      // 	gcurv_min = d;
      
	      tcurvs.push_back(d);
	      curvs.push_back(tcurvs);
	    }
			
	  verts.clear();
	  faces.clear();
	  edgef.clear();
	  edgeb.clear();
	  cu_time += st_time;
  	  num_st++;

	  // mcurv_min -= 0.001;
	  // mcurv_max += 0.001;
	  mcurv_min = -1.2;
	  mcurv_max = 1.9;
	  // gcurv_min -= 0.001;
	  // gcurv_max += 0.001;
	  gcurv_min = -1.2;
	  gcurv_max = 1.9;

	  bin_width1 = (mcurv_max - mcurv_min) / nbins;
	  bin_width2 = (gcurv_max - gcurv_min) / nbins;
	  for(i = 0;i < curvs.size();i++)
	    {
	      //  cout<<(bls[i]-bl_min)/bin_width<<endl;
	      //cout<<(int)((curvs[i][0] - mcurv_min)/bin_width1)<<"\t"<<(int)((curvs[i][1] - gcurv_min)/bin_width2)<<endl;
	      curv_dist[(int)((curvs[i][0] - mcurv_min) / bin_width1)][(int)((curvs[i][1] - gcurv_min) / bin_width2)]++;
	    }

	  sprintf(outfile, "%s/curv_dist_%d.dat", argv[f], cu_time);
	  fout.open(outfile, ios::out);
	  for(i = 0;i < nbins;i++)
	    {
	      for(j = 0; j < nbins;j++)
		{
		  fout<<mcurv_min + bin_width1*i<<"\t"<<gcurv_min + bin_width2*j<<"\t"<<((double) curv_dist[i][j])/((double)curvs.size())<<endl;
		}
	      fout<<endl;
	    }
	  fout.close();
      
	  // Clear everything
	  curvs.clear();
	  curv_dist.clear();

  	}	
      cout<<"\n";
    }

  cout<<"\nDone."<<endl;
  return 0;
}


#ifndef __EXTRAS_HPP__
#define __EXTRAS_HPP__

#include <cmath>
#include "data_structures.hpp"

void restartFromVTKFile(char *);
void restartFromVTPFile(char *);
void restartOrdering();
void ordering();
int lengthCheck(Vert *, Vert *);


//void buildCellList();
#endif

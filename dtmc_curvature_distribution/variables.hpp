/*

Global variables in the simulation

*/

#ifndef VARIABLES_HPP
#define VARIABLES_HPP

#include <vector>
#include <random>
#include "data_structures.hpp"

extern vector <Vert *> verts;						// List of all vertices
extern vector <Face *> faces; 					// List of all Triangles
extern vector <Edge *> edgef; 						// List of all Links
extern vector <Edge *> edgeb; 						// List of all oppositely oriented links

//extern vector < vector <int> > cellList;
//extern vector < vector <int> > neighList;

extern Vert tdv[10]; 							// To store temporary vertices
extern Face tdt[10]; 							// To store temporary triangles
extern Edge tdl[10];								// To store temporary links

extern Vert *new_vertex;	 					// Place holder for a new vertex
extern Face *new_tri; 							// Place holder for a new triangle
extern Edge *new_li; 								// Place holder for new link
extern Edge *new_mli;
extern Edge *tmpLi;
extern Edge *tmpMli;
extern Face *tmpTri;

extern mt19937 generator;
extern unsigned int GEN_MAX;
extern unsigned int GEN_MIN;
extern double mem_elastic_energy; 					// Elastic energy of the membrane
extern double mem_area; 							// Total area of the membrane
extern double mem_volume; 							// Total volume of the membrane
extern double mem_enp;								// The pV energy of the vesicle
extern double mem_total_energy; 					// Total energy of the membrane
extern double total_mean_curvature; 				// Total mean curvature of the membrane
extern double total_phi;

#endif

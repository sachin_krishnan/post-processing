/*

Definition of the data structures

*/

#include "data_structures.hpp"
#include "extras.hpp"
#include <cstdio>
#include <cmath>

Vert::Vert() : id(0), phi(0), num_neighbors(0), mean_curvature(0.0), area(0.0), //cell{0, 0, 0},
	       normal{0.0, 0.0, 0.0}, coord{0.0, 0.0,0.0},
	       vertex{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}, edge{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
{} // Clean initialization of Vertex object

Vert::~Vert()
{
  for(int i = 0;i < num_neighbors;i++)
    {
      this->vertex[i] = NULL;
      this->edge[i] = NULL;
    }
}

void Vert::setCoord(double a, double b, double c) 	// Set vertex coordinates in one line
{
  this->coord[0] = a;
  this->coord[1] = b;
  this->coord[2] = c;
}

void Vert::calculateArea()
{
  int i;
  this->area = 0.0;
  for(i = 0;i < this->num_neighbors;i++)
    {
      this->area += this->edge[i]->face->area;
    }
  this->area *= 0.333333333;
}

/* This is a simple curvature calculation. Doesn't give Gaussian curvature

void Vert::calculateCurvature()
{
  int i;
  int im;
  int ip;
  // double lij_2;
  double v0[3];
  double v1[3];						
  double v2[3];						
  double v3[3];						
  double v4[3];						
  double v5[3];						
  double cot_theta[2];		
  double dot;
  double cross;
  double sigma;
  double fwei = 0.0;
  double sum[3] = {0.0, 0.0, 0.0};
  double nor[3] = {0.0, 0.0, 0.0};
  double aij;
  double nor_2;
	
  for(i = 0;i < this->num_neighbors;i++)
    {
      fwei += this->edge[i]->face->area;
      im = (i + this->num_neighbors - 1) % this->num_neighbors;
      ip = (i + 1) % this->num_neighbors;

      v1[0] = this->coord[0] - this->vertex[i]->coord[0];
      v1[1] = this->coord[1] - this->vertex[i]->coord[1];
      v1[2] = this->coord[2] - this->vertex[i]->coord[2];

      v2[0] = this->coord[0] - this->vertex[im]->coord[0];
      v2[1] = this->coord[1] - this->vertex[im]->coord[1];
      v2[2] = this->coord[2] - this->vertex[im]->coord[2];

      v3[0] = this->coord[0] - this->vertex[ip]->coord[0];
      v3[1] = this->coord[1] - this->vertex[ip]->coord[1];
      v3[2] = this->coord[2] - this->vertex[ip]->coord[2];

      v4[0] = v2[0] - v1[0];
      v4[1] = v2[1] - v1[1];
      v4[2] = v2[2] - v1[2];

      v5[0] = v3[0] - v1[0];
      v5[1] = v3[1] - v1[1];
      v5[2] = v3[2] - v1[2];

      // lij_2 = (v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);

      dot = v3[0] * v5[0] + v3[1] * v5[1] + v3[2] * v5[2];
      v0[0] = v3[1] * v5[2] - v3[2] * v5[1];
      v0[1] = v3[2] * v5[0] - v3[0] * v5[2];
      v0[2] = v3[0] * v5[1] - v3[1] * v5[0];
      cross = sqrt(v0[0] * v0[0] + v0[1] * v0[1] + v0[2] * v0[2]);
      cot_theta[0] = dot/cross;

      dot = v2[0] * v4[0] + v2[1] * v4[1] + v2[2] * v4[2];
      v0[0] = v2[1] * v4[2] - v2[2] * v4[1];
      v0[1] = v2[2] * v4[0] - v2[0] * v4[2];
      v0[2] = v2[0] * v4[1] - v2[1] * v4[0];
      cross = sqrt(v0[0] * v0[0] + v0[1] * v0[1] + v0[2] * v0[2]);
      cot_theta[1] = dot/cross;

      aij = (cot_theta[0] + cot_theta[1]);
      // bij = lij_2 * aij;
      // sigma += bij;
      sum[0] += (v1[0] * aij);
      sum[1] += (v1[1] * aij);
      sum[2] += (v1[2] * aij);
    }

  // sigma *= 0.25;
  fwei = 1.0 / fwei;
	
  for(i = 0;i < this->num_neighbors;i++)
    {
      nor[0] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[0];
      nor[1] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[1];
      nor[2] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[2];
    }

  nor_2 = sqrt(nor[0] * nor[0] + nor[1] * nor[1] + nor[2] * nor[2]);
  nor[0] /= nor_2;
  nor[1] /= nor_2;
  nor[2] /= nor_2;
  this->normal[0] = nor[0];
  this->normal[1] = nor[1];
  this->normal[2] = nor[2];
  sigma = 2.0 * this->area;
  // 0.5 from aij formula (optimization)
  // using sigma = 2.0 * A, because we need mean curvature here. The formula in Nelson, Piran, Weinberg calculates total curvature
  this->mean_curvature = 0.5 * (nor[0] * sum[0] + nor[1] * sum[1] + nor[2] * sum[2]) / sigma;
}
*/

void Vert::calculateCurvature()
{
  int i;
  int j;
  int k;
  int im, ip;
  double p;
  double q;
  double r;
  double s;
  double si;
  double e1;
  double e2;
  double fwei;
  double nor_sq;
  double en_sq;
  double amat_sq;
  double smat_sq;
  double rtan_sq;
  double elen;
  double dih;
  double emcu;
  double insq;
  double unit_matrix[3][3];
  double an;
  double wei;
  double xc;
  double evec[3][3];
  double pmatrix[3][3];
  double hmatrix[3][3];
  double cmatrix[3][3];
  double tmatrix[3][3];
  double nor[3];
  double v1[3];
  double v2[3];
  double cpm[3];
  double fn1[3];
  double fn2[3];
  double en[3];
  double rtan[3];
  double amat[3];
  double smat[3];
  double zdir[3];
  double curvature1;
  double curvature2;
  
  fwei = 0.0;
  zdir[0] = 0.0;
  zdir[1] = 0.0;
  zdir[2] = 1.0;
  for(i = 0;i < 3;i++)
  {
    nor[i] = 0.0;
    for(j = 0;j < 3;j++)
    {
      cmatrix[i][j] = 0.0;
      hmatrix[i][j] = 0.0;
      tmatrix[i][j] = 0.0;
      evec[i][j] = 0.0;
      unit_matrix[i][j] = (j==i)?1.0:0.0;
    }
  }

  for(i = 0;i < this->num_neighbors;i++)
  {
    fwei += this->edge[i]->face->area;
  }
  fwei = 1.0 / fwei;
  for(i = 0;i < this->num_neighbors;i++)
  {
    nor[0] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[0];
    nor[1] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[1];
    nor[2] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[2];
  }

  nor_sq = sqrt(nor[0] * nor[0] + nor[1] * nor[1] + nor[2] * nor[2]);
  nor[0] /= nor_sq;
  nor[1] /= nor_sq;
  nor[2] /= nor_sq;
  this->normal[0] = nor[0];
  this->normal[1] = nor[1];
  this->normal[2] = nor[2];

  for(i = 0;i < 3;i++)
    for(j = 0;j < 3;j++)
      pmatrix[i][j] = unit_matrix[i][j] - nor[i] * nor[j];
  
  for(i = 0;i < this->num_neighbors;i++)
  {
    im = i - 1;
    if(i == 0) im = this->num_neighbors-1;
    ip = i + 1;
    if(i == this->num_neighbors - 1) ip = 0;

    v2[0] = this->vertex[i]->coord[0] - this->coord[0];
    v2[1] = this->vertex[i]->coord[1] - this->coord[1];
    v2[2] = this->vertex[i]->coord[2] - this->coord[2];

    elen = sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2]);

    v2[0] /= elen;
    v2[1] /= elen;
    v2[2] /= elen;

    // where is ip?

    fn1[0] = this->edge[im]->face->normal[0];
    fn1[1] = this->edge[im]->face->normal[1];
    fn1[2] = this->edge[im]->face->normal[2];

    fn2[0] = this->edge[i]->face->normal[0];
    fn2[1] = this->edge[i]->face->normal[1];
    fn2[2] = this->edge[i]->face->normal[2];

    an = fn1[0] * fn2[0] + fn1[1] * fn2[1] + fn1[2] * fn2[2];
    
    if(an > 1.0) 
      an = 1.0000000;

    v1[0] = fn1[1]*fn2[2] - fn1[2]*fn2[1];
    v1[1] = fn1[2]*fn2[0] - fn1[0]*fn2[2];
    v1[2] = fn1[0]*fn2[1] - fn1[1]*fn2[0];

    si = 1.0;

    xc = v2[0] * v1[0] + v2[1] * v1[1] + v2[2] * v1[2];

    if(xc < 0.0) 
      si = -1.0;

    dih = PI + si * acos(an);

    en[0] = this->edge[im]->face->normal[0] + this->edge[i]->face->normal[0];
    en[1] = this->edge[im]->face->normal[1] + this->edge[i]->face->normal[1];
    en[2] = this->edge[im]->face->normal[2] + this->edge[i]->face->normal[2];

    en_sq = sqrt(en[0] * en[0] + en[1] * en[1] + en[2] * en[2]);

    en[0] /= en_sq;
    en[1] /= en_sq;
    en[2] /= en_sq;

    wei = nor[0] * en[0] + nor[1] * en[1] + nor[2] * en[2];

    emcu = 2.0 * elen * cos(dih * 0.5);

    cpm[0] = v2[1] * en[2] - v2[2] * en[1];
    cpm[1] = v2[2] * en[0] - v2[0] * en[2];
    cpm[2] = v2[0] * en[1] - v2[1] * en[0];

    for(k = 0;k < 3;k++)
    {
      rtan[k] = pmatrix[k][0] * cpm[0] + pmatrix[k][1] * cpm[1] + pmatrix[k][2] * cpm[2]; 
    }

    rtan_sq = sqrt(rtan[0] * rtan[0] + rtan[1] * rtan[1] + rtan[2] * rtan[2]);

    rtan[0] /= rtan_sq;
    rtan[1] /= rtan_sq;
    rtan[2] /= rtan_sq;

    for(k = 0;k < 3;k++)
    {
      for(j = 0;j < 3;j++)
      {
        cmatrix[k][j] += (0.5 * wei * emcu * rtan[k] * rtan[j]);
      }
    }
  }

  amat[0] = zdir[0] + nor[0];
  amat[1] = zdir[1] + nor[1];
  amat[2] = zdir[2] + nor[2];

  smat[0] = zdir[0] - nor[0];
  smat[1] = zdir[1] - nor[1];
  smat[2] = zdir[2] - nor[2];

  amat_sq = sqrt(amat[0] * amat[0] + amat[1] * amat[1] + amat[2] * amat[2]);
  smat_sq = sqrt(smat[0] * smat[0] + smat[1] * smat[1] + smat[2] * smat[2]);

  if(amat_sq > smat_sq)
  {
    amat[0] /= amat_sq;
    amat[1] /= amat_sq;
    amat[2] /= amat_sq;

    for(i = 0;i < 3;i++)
    {
      for(j = 0;j < 3;j++)
      {
        hmatrix[i][j] = - (unit_matrix[i][j] - 2.0 * amat[i] * amat[j]);
      }
    }
  }
  else
  {
    smat[0] /= smat_sq;
    smat[1] /= smat_sq;
    smat[2] /= smat_sq;

    for(i = 0;i < 3;i++)
    {
      for(j = 0;j < 3;j++)
      {
        hmatrix[i][j] = (unit_matrix[i][j] - 2.0 * smat[i] * smat[j]);
      }
    }
  }

  for(i = 0;i < 3;i++)
  {
    for(j = 0;j < 3;j++)
    {
      tmatrix[i][j] = cmatrix[i][0] * hmatrix[0][j] + cmatrix[i][1] * hmatrix[1][j] + cmatrix[i][2] * hmatrix[2][j];
    }
  }
  
  for(i = 0;i < 3;i++)
  {
    for(j = 0;j < 3;j++)
    {
      pmatrix[i][j] = hmatrix[0][i] * tmatrix[0][j] + hmatrix[1][i] * tmatrix[1][j] + hmatrix[2][i] * tmatrix[2][j];
    }
  }

  p = pmatrix[0][0];
  q = pmatrix[0][1];
  r = pmatrix[1][0];
  s = pmatrix[1][1];

  if(q != 0.0 && r != 0.0)
  {
    insq = (p + s) * (p + s) - 4.0 * (p * s - q * r);
    if(insq > 0.0)
    {
      if((p + s) < 0.0)
      {
        e1 = ((p + s) - sqrt(insq)) * 0.5;
        e2 = ((p + s) + sqrt(insq)) * 0.5;
      }
      else
      {
        e1 = ((p + s) + sqrt(insq)) * 0.5;
        e2 = ((p + s) - sqrt(insq)) * 0.5;
      }
    }
    else
    {
      e1 = (p + s) * 0.5;
      e2 = e1;
    }
  }
  else
  {
    if(fabs(p) > fabs(s))
    {
      e1 = p;
      e2 = s;
      evec[0][0] = evec[1][1] = evec[2][2] = 1.0;
    }
    else
    {
      e1 = s;
      e2 = p;
    }
  }

  if(fabs(e1) < 1e-11) e1 = 0.0;
  if(fabs(e2) < 1e-11) e2 = 0.0;

  curvature1 = e1/this->area;
  curvature2 = e2/this->area;
  this->gaussian_curvature = (curvature1 * curvature2);
  this->mean_curvature = (curvature1 + curvature2) * 0.5;
}

void Vert::removeNeigh(Vert *ve)
{
  int i, j;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve)
	{
	  for(j = i; j < this->num_neighbors - 1; j++)
	    {
	      this->vertex[j] = this->vertex[j+1];
	      this->edge[j] = this->edge[j+1];
	    }
	  i = this->num_neighbors;
	}
    }
  this->num_neighbors--;
  this->vertex[this->num_neighbors] = NULL;
  this->edge[this->num_neighbors] = NULL;
}

void Vert::replaceNeigh(Vert *ve1, Vert *ve2, Edge *lf)
{
  int i;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve1)
	{
	  this->vertex[i] = ve2;
	  this->edge[i] = lf;
	  i = this->num_neighbors;
	}
    }
}

void Vert::addNeighAfter(Vert *ve1, Vert *ve2, Edge *lf)
{
  int i;
  int j;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve1)
	{
	  for(j = this->num_neighbors;j > i + 1;j--)
	    {
	      this->vertex[j] = this->vertex[j-1];
	      this->edge[j] = this->edge[j-1];
	    }
	  this->vertex[i + 1] = ve2;
	  this->edge[i + 1] = lf;
	  i = this->num_neighbors;
	}
    }
  this->num_neighbors++;
}
/*
void Vert::findCell()
{
  this->cell[0] = (this->coord[0] - XLO) / NEIGH_CUT_OFF;
  this->cell[1] = (this->coord[1] - YLO) / NEIGH_CUT_OFF;
  this->cell[2] = (this->coord[2] - ZLO) / NEIGH_CUT_OFF;
}
*/
Face::Face() : area(0.0), volume(0.0), normal{0.0, 0.0, 0.0}, //cell{0, 0, 0},
		       vertex{NULL, NULL, NULL}, edge{NULL, NULL, NULL}
{} // Clean initialization of Face object

Face::~Face()
{
  this->vertex[0] = NULL;
  this->vertex[1] = NULL;
  this->vertex[2] = NULL;
  this->edge[0] = NULL;
  this->edge[1] = NULL;
  this->edge[2] = NULL;
}

/* Face angle check. */
int Face::faceAngleCheck()
{
  double npro;
  for(int k = 0;k < 3;k++)
    {
      npro = this->normal[0] * this->edge[k]->opposite->face->normal[0] + this->normal[1] * this->edge[k]->opposite->face->normal[1] + this->normal[2] * this->edge[k]->opposite->face->normal[2];
      if(npro < FANGLE)
	return 1;
    }
  return 0;
}

/* Calculate area, volume and normal for a triangle */
int Face::calculateArea()
{
  double ax;
  double ay;
  double az;
  double r1[3];
  double r2[3];
  double r3[3];
  double r21[3];
  double r31[3];
  
  for(int l = 0;l < 3;l++)
    {
      r1[l] = this->vertex[0]->coord[l];
      r2[l] = this->vertex[1]->coord[l];
      r3[l] = this->vertex[2]->coord[l];
    }

  r21[0] = r2[0] - r1[0];
  r21[1] = r2[1] - r1[1];
  r21[2] = r2[2] - r1[2];
  
  r31[0] = r3[0] - r1[0];
  r31[1] = r3[1] - r1[1];
  r31[2] = r3[2] - r1[2];
  
  ax = (r21[1] * r31[2] - r21[2] * r31[1]) * 0.5;
  ay = (r21[2] * r31[0] - r21[0] * r31[2]) * 0.5;
  az = (r21[0] * r31[1] - r21[1] * r31[0]) * 0.5;
  this->area = sqrt(ax*ax + ay*ay + az*az);
  if(this->area < 0.43)
    return 1;
  
  ax /= this->area;
  ay /= this->area;
  az /= this->area;
  
  this->normal[0] = ax;
  this->normal[1] = ay;
  this->normal[2] = az;
  
  this->volume = (r1[0] * (r2[1] * r3[2] - r2[2] * r3[1]) + r1[1] * (r2[2] * r3[0] - r2[0] * r3[2]) + r1[2] * (r2[0] * r3[1] - r2[1] * r3[0])) / 6.0;
  return 0;
}

/*
void Face::findCell()
{
  double centroid[3];
  centroid[0] = (this->vertex[0]->coord[0] + this->vertex[1]->coord[0] + this->vertex[2]->coord[0]) * 0.333333333;
  centroid[1] = (this->vertex[0]->coord[1] + this->vertex[1]->coord[1] + this->vertex[2]->coord[1]) * 0.333333333;
  centroid[2] = (this->vertex[0]->coord[2] + this->vertex[1]->coord[2] + this->vertex[2]->coord[2]) * 0.333333333;
  this->cell[0] = (centroid[0] - XLO) / NEIGH_CUT_OFF;
  this->cell[1] = (centroid[1] - YLO) / NEIGH_CUT_OFF;
  this->cell[2] = (centroid[2] - ZLO) / NEIGH_CUT_OFF;
}
*/
Edge::Edge() : face(NULL), vertex{NULL, NULL}, opposite(NULL)
{} // Clean initialization of Edge object

Edge::~Edge()
{
  this->face = NULL;
  this->vertex[0] = NULL;
  this->vertex[1] = NULL;
  this->opposite = NULL;
}

int Edge::lengthCheck()
{
  double v21[3];
  
  v21[0] =  this->vertex[0]->coord[0] - this->vertex[1]->coord[0];
  v21[1] =  this->vertex[0]->coord[1] - this->vertex[1]->coord[1];
  v21[2] =  this->vertex[0]->coord[2] - this->vertex[1]->coord[2];
  double dis = v21[0] * v21[0] + v21[1] * v21[1] + v21[2] * v21[2];
  if(dis <= 1.0 || dis > 3.0)
    return 1;
  return 0;
}

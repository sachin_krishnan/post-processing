/*

Global parameters used in the simulation

*/

#ifndef __PARAMETERS_HPP__

#define __PARAMETERS_HPP__

#define MACH_EPS 1e-10
#define PI 3.141592653589793 		

/* Parameters for triangulated surface model*/
#define BLEN 1.3 // Initial fixed bond length used in generating triangulated tetrahedron
#define BETA 1.0 // 1 / (kB * T)
#define FANGLE -0.5 // Maximum allowed angle between adjacent faces
#define SP_CUR 0.0 // Spontaneous Curvature
#define IN_PLANE_FLUIDITY 0.3 //Bond flip to vertex displacement ratio

#define XLO -20.0
#define YLO -20.0
#define ZLO -20.0

#define NEIGH_CUT_OFF 4.0

#define MINIMUM_TRIANGLE_VERTEX_DISTANCE 0.5

extern int NOLI; // Number of rows of tirangles in one face of the initial tetrahedron geometry
extern int SEED; // Initial random seed 
extern unsigned long FTIME; // Total number of MC Steps
extern double KAPPA; // Bending modulus of the membrane
extern double PRESSURE; // Excess pressure inside the vesicle
extern char OUTDIR[512]; // Generate output in this directory
extern int OUTPUT_FREQ; // Frequency of generating output files / data 
extern int NEIGH_FREQ;  // Frequency of updating neighbor list

extern double PROTEIN_SP_CUR;
extern double CHEMICAL_POTENTIAL;
extern double CURV_SENS;
#endif

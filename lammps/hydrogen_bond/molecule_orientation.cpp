#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;

std::streampos fileSize( const char* filePath ){

    std::streampos fsize = 0;
    std::ifstream file( filePath, std::ios::in );

    fsize = file.tellg();
    file.seekg( 0, std::ios::end );
    fsize = file.tellg() - fsize;
    file.close();

    return fsize;
}

void doit(char *inpfile)
{
	int i;
	int j;
	int step;
	int count=0;
	int numofatoms=0;
	int id=0;
	int typ=0;
	int pos=-1;
	int mol=0, molid=0;
	double ftemp=0.0;
	bool bounds=false;
	double xlo,xhi,ylo,yhi,zlo,zhi;
	double boxx,boxy,boxz;
	double x,y,z;
	char outf1[256];	
	char outf2[256];
	char outf3[256];
	string temp;
	string junk;
	fstream fp;
	fstream outfile1, outfile2, outfile3;
	vector < vector < double > > r;
	fp.open(inpfile);
	if(fp.bad()) 
		cout<<"Error opening the file."<<endl;
	strcpy(outf1,"");
	strcat(outf1,inpfile);
	strcat(outf1,".orient.x");
	strcpy(outf2,"");
	strcat(outf2,inpfile);
	strcat(outf2,".orient.y");
	strcpy(outf3,"");
	strcat(outf3,inpfile);
	strcat(outf3,".orient.z");
	outfile1.open(outf1,ios::out);
	outfile2.open(outf2,ios::out);
	outfile3.open(outf3,ios::out);
//	if(outfile1.bad()) 
//		cout<<"Error creating output file."<<endl;
	cout<<"Input file :"<<inpfile<<endl<<"Reading file..."<<endl;
//	outfile1<<"#Input file :"<<inpfile<<endl;
	outfile1<<"#angle count\n";
	outfile2<<"#angle count\n";
	outfile3<<"#angle count\n";
	int xcount[180], ycount[180], zcount[180];
	while(!fp.eof())
	{
		fp>>temp;
		if(strcmp(temp.c_str(),"ITEM:")==0)
		{
			fp>>temp;		
			if(strcmp(temp.c_str(),"TIMESTEP")==0)
			{
				fp>>step;
			}
			else if(strcmp(temp.c_str(),"NUMBER")==0)
			{
				fp>>junk>>junk;
				fp>>numofatoms;	
				for(i = 0;i < numofatoms / 3;i++)
				{
					r.push_back(vector <double>());
					for(j = 0;j < 9;j++)
					{
						r[i].push_back(0.0);
					}
				}
			}else if(strcmp(temp.c_str(),"BOX")==0 && !bounds)
			{
				fp>>junk>>junk>>junk>>junk;
				fp>>xlo>>xhi>>ylo>>yhi>>zlo>>zhi;
				boxx=xhi-xlo;
				boxy=yhi-ylo;
				boxz=zhi-zlo;
				bounds=true;				
			}
			else if(strcmp(temp.c_str(),"ATOMS")==0)
			{
				//id mol type x y z vx vy vz 
				fp>>junk>>junk>>junk>>junk>>junk>>junk>>junk>>junk>>junk;
				for(i=0;i<numofatoms;i++)
				{
					fp>>id>>mol>>typ>>x>>y>>z>>ftemp>>ftemp>>ftemp;
					r[mol-1][(((id-1)%3))*3] = x;
					r[mol-1][(((id-1)%3))*3+1] = y;
					r[mol-1][(((id-1)%3))*3+2] = z;
				}
				double vec1[3], vec2[3];
				double norm1, norm2;
				double mol_vec[3];
				double alpha, beta, gamma;
				for(i = 0;i < r.size();i++)
				{
					vec1[0] = r[i][3] - r[i][0];
					vec1[1] = r[i][4] - r[i][1];
					vec1[2] = r[i][5] - r[i][2];

					vec2[0] = r[i][6] - r[i][0];
					vec2[1] = r[i][7] - r[i][1];
					vec2[2] = r[i][8] - r[i][2];
	
					norm1 = sqrt(vec1[0] * vec1[0] + vec1[1] * vec1[1] + vec1[2] * vec1[2]);
					norm2 = sqrt(vec2[0] * vec2[0] + vec2[1] * vec2[1] + vec2[2] * vec2[2]);

					vec1[0] /= norm1;
					vec1[1] /= norm1;
					vec1[2] /= norm1;

					vec2[0] /= norm2;
					vec2[1] /= norm2;
					vec2[2] /= norm2;
		
					mol_vec[0] = (vec1[0] + vec2[0]) * 0.5;
					mol_vec[1] = (vec1[1] + vec2[1]) * 0.5;
					mol_vec[2] = (vec1[2] + vec2[2]) * 0.5;

					alpha = acos(mol_vec[0]) * 180.0 / M_PI;
					beta = acos(mol_vec[1]) * 180.0 / M_PI;
					gamma = acos(mol_vec[2]) * 180.0 / M_PI;
					
					xcount[(int)alpha]++;
					ycount[(int)beta]++;
					zcount[(int)gamma]++;
				}
				for(i = 0;i < 180;i++)
				{
					outfile1<<i<<" "<<xcount[i]<<endl;
					outfile2<<i<<" "<<ycount[i]<<endl;
					outfile3<<i<<" "<<zcount[i]<<endl;
				}
			}
		}
	}
	cout<<"Done...\n";		
	fp.close();
	outfile1.close();
	outfile2.close();
	outfile3.close();
}

int main(int argc, char **argv)
{
	int i;
	if(argc>=2)
	{
		for(i = 1;i < argc;i++)
			doit(argv[i]);
	}
	else
		cout<<"Arguments: <input-file(s)>"<<endl;
	return 0;
}

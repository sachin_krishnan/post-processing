#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

#define EPS 1e-10

using namespace std;

int main(int argc, char *argv[])
{
  int i, j, k, l;
  stringstream iss;
  double **r;
  int nump;
  int in_time;  // Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;  // Final timestep (* in mem.*.vtp)  Eg. 500 fi_time >= in_time
  int st_time;  // Steps between consecutive timesteps Eg. 10
  int cu_time;  // Stores the current timestep
  double rx, ry, rz;
  string temp;
  string line;
  double gyration[3][3];
  double cr[3];
  double eig[3];
  double rg;
  double as;
  char filename[256];
  ofstream outfile;

  sprintf(filename, "%s/gyration.dat", argv[4]);
  outfile.open(filename, ios::out);
  outfile<<"#file\tlambda_x^2\tlambda_y^2\tlambda_z^2\tR_g\tasphericity\n"; 
  if(argc < 5)
    {
      cout<<"Usage : <exec> <initial-time> <final-time> <step-time> <data-folder>\n";
      return 0;
    }
  
  in_time = atoi(argv[1]);
  fi_time = atoi(argv[2]);
  st_time = atoi(argv[3]);
  
  cu_time = in_time;
  cout<<"Processing : "<<in_time<<" / "<<fi_time;
  // Processing the files one at a time
  while(cu_time <= fi_time)
    {
      /* Progress */
      cout<<"\rProcessing : "<<cu_time<<" / "<<fi_time<<"\t";
      fflush(stdout);
      /* Input */
      sprintf(filename, "%s/mem.%d.vtp",argv[4],cu_time);
      ifstream infile;
      infile.open(filename, ios::in); 
      while(!infile.eof())
	{
	  infile>>temp;
	  if(strcmp(temp.c_str(),"<Piece") == 0)
	    {
	      infile>>temp;
	      iss.str(temp);
	      getline(iss,line,'\"');
	      getline(iss,line,'\"');
	      nump = atoi(line.c_str());
	      r = (double **) malloc(sizeof(double *) * nump);
	      for(i = 0;i < nump;i++)
		r[i] = (double *) malloc(sizeof(double) * 3);
	    }
	  else if(strcmp(temp.c_str(),"<Points>")==0)
	    {
	      for(i = 0;i < 5;i++)
		infile>>line;
	      cr[0] = 0.0;
	      cr[1] = 0.0;
	      cr[2] = 0.0;
	      
	      for(i = 0;i < nump;i++)
		{ 
		  infile>>rx>>ry>>rz;
		  r[i][0] = rx;
		  r[i][1] = ry;
		  r[i][2] = rz;
		  cr[0] += rx;
		  cr[1] += ry;
		  cr[2] += rz;
		}
	      //cout<<"Done reading point data.\n";
	    }
	}
      infile.close();
      
      /* COM coordinates*/
      cr[0] /= static_cast<double>(nump);
      cr[1] /= static_cast<double>(nump);
      cr[2] /= static_cast<double>(nump);

      //cout<<"COM is located at : ("<<cr[0]<<", "<<cr[1]<<", "<<cr[2]<<endl;
  
      /* Translate the origin */
      for(i = 0;i < nump;i++)
	{
	  for(j = 0;j < 3;j++)
	    {
	      r[i][j] -= cr[j];
	    }
	}
      
      /* Processing */
      for(i = 0;i < 3;i++)
	{
	  for(j = 0;j < 3;j++)
	    {
	      gyration[i][j] = 0.0;
	      for(k = 0;k < nump;k++)
		{
		  for(l = 0;l < nump;l++)
		    {
		      gyration[i][j] += (r[k][i] - r[l][i]) * (r[k][j] - r[l][j]);
		    }
		}
	      gyration[i][j] /= (2.0 * nump * nump);
	      if(abs(gyration[i][j]) < EPS)
		gyration[i][j] = 0.0;
	    }
	}
      
      /* Output */
      /*  cout<<"Gyration tensor -> \n";
      for(i = 0;i < 3;i++)
	{
	  for(j = 0;j < 3;j++)
	    {
	      cout<<gyration[i][j]<<"\t\t";
	    }
	  cout<<endl;
	}
      */
      gsl_vector *eval = gsl_vector_alloc(3);
      gsl_matrix *evec = gsl_matrix_alloc(3, 3);
      gsl_matrix *gyr = gsl_matrix_alloc(3, 3);
      
      for(i = 0;i < 3;i++)
	{
	  for(j = 0; j < 3;j++)
	    {
	      gsl_matrix_set(gyr, i, j, gyration[i][j]);
	    }
	}
  
      gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(3);
      
      gsl_eigen_symmv(gyr, eval, evec, w);
      
      gsl_eigen_symmv_sort (eval, evec, GSL_EIGEN_SORT_ABS_ASC);
      eig[0] = gsl_vector_get(eval, 0);
      eig[1] = gsl_vector_get(eval, 1);
      eig[2] = gsl_vector_get(eval, 2);
      //cout<<"Eigenvalues are : "<<eig[0]<<", "<<eig[1]<<", "<<eig[2]<<endl;

      rg = sqrt(eig[0] + eig[1] + eig[2]);
      //cout<<"Rg = "<<rg<<endl;
      
      as = eig[2] - 0.5 * (eig[0] + eig[1]);
      if(as < EPS) as = 0.0;
      //cout<<"Asphericity = "<<as<<endl;

      /* Write to file */
      outfile<<cu_time<<"\t"<<eig[0]<<"\t"<<eig[1]<<"\t"<<eig[2]<<"\t"<<rg<<"\t"<<as<<endl;
      
      /* Cleanup */
      gsl_eigen_symmv_free(w);
      gsl_matrix_free(gyr);
      gsl_matrix_free(evec);
      gsl_vector_free(eval);
      
      for(i = 0;i < nump;i++)
	{
	  if(r[i] != NULL)
	    free(r[i]);
	}
      if(r != NULL)
	free(r);

      cu_time += st_time;
    }

  outfile.close();
  cout<<endl<<"Done!"<<endl;
  return 0;
}
  

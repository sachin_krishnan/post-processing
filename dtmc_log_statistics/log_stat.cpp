#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

int main(int argc, char *argv[])
{
  int f;
  int i;
  int j;
  int k;
  int in_time;	// Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;	// Final timestep (* in mem.*.vtp)	Eg. 500 fi_time >= in_time
  int cu_time;	// Stores the current timestep
  int junk;	
  int num_st;
  //  int nbins;
  int OUT_FREQ;	// Output frequency in the MC code
  int column;
  double var = 0;
  vector <double> vals;
  
  // Taking input through command line arguments
  if(argc < 5)
    {	
      cout<<"Usage : <exec> <column> <initial_time> <final_time> <data-folder(s)"<<endl;
      return 0;
    }
  column = atoi(argv[1]);
  in_time = atoi(argv[2]);
  fi_time = atoi(argv[3]);

  double progress;
  double val;
  double sd;
  string sjunk;
  char infile[256];
  string temp;
  string line;
  ifstream fp;
  stringstream iss;
  
  //Finding mean radius from mean volume by processing memprops.dat
  k = 0;
  sd = 0.0;
  char fnames[256];

  for(f = 4;f < argc;f++)
    {
      sprintf(fnames, "%s/memprops.dat",argv[f]);
      fp.open(fnames, ios::in);
      while(getline(fp,line))   
  	{
	  stringstream iss(line);
	  if(fp.eof())
	    break;
	  if(line.c_str()[0] != '#')
	    {
	      getline(iss, sjunk, ' ');
	      cu_time = atoi(sjunk.c_str());
	      for(i = 0;i < column;i++)
		getline(iss, sjunk, ' ');
	      //getline(iss, sjunk, ' ');
	      //getline(iss, sjunk, ' ');
	      val = atof(sjunk.c_str());
				
	      if(cu_time >= in_time && cu_time <= fi_time && val!=0.0)
		{
//		  cout<<val<<endl;
	    	  sd += val;
		  vals.push_back(val);
		  k++;
		}
	    }
  	}
      fp.close();
    }
  sd /= (double) k;
  
  cout<<"Mean = "<<sd<<endl;

  var = 0.0;
  for(k = 0;k < vals.size();k++)
    {
      var += (vals[k] - sd) * (vals[k] - sd);
    }
  var /= vals.size();

  cout<<"Variance = "<<var<<endl;
  
  sd = sqrt(var);
  cout<<"Standard Deviation = "<<sd<<endl;
 	
  // Clear everything
  cout<<"\nDone."<<endl;
  vals.clear();
  return 0;
}

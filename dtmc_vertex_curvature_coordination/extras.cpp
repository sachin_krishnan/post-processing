
#include "extras.hpp"
#include <fstream>
#include <vector>
#include <sstream>
#include <cstring>
#include <algorithm>
#include "variables.hpp"

// Start from a restart file (VTP format) 
void restartFromVTPFile(char *fileName)
{
  int i;
  int j;
  int k;
  int k1;
  int kp1;
  int km1;
  int l;
  int l1;
  int lp1;
  int m;
  int ord_count;
  int nump = 0;
  int numl = 0;
  int numt = 0;
  int id1;
  int id2;
  int id3;
	int phi;
  bool flag;
  bool done_flag;
  double rx;
  double ry;
  double rz;
  string line;
  string temp;
  ifstream infile;
  stringstream iss;
  bool true_neigh;
  bool *ordchk;
  Vert *ver_tmp;
  Edge *li_tmp;
  
  infile.open(fileName, ios::in);

  /* Reading the header */
  done_flag = false;
  while(!infile.eof() && !done_flag)
  {
    infile>>line;
    if(strcmp(line.c_str(),"<Piece")==0)
		{
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
	  	getline(iss,temp,'\"');
		  nump = atoi(temp.c_str());
		  infile>>line;
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
	  	getline(iss,temp,'\"');
		  numl = atoi(temp.c_str());
		  infile>>line;
	  	infile>>line;
		  iss.str(line);
		  getline(iss,temp,'\"');
		  getline(iss,temp,'\"');
		  numt = atoi(temp.c_str());
		}
    else if(strcmp(line.c_str(),"<Points>")==0)
		{
	  	for(i = 0;i < 5;i++)
	    	infile>>line;
		  for(i = 0;i < nump;i++)
	    {
	      infile>>rx>>ry>>rz;
	      new_vertex = new Vert();
	      verts.push_back(new_vertex);
	      new_vertex->id = i;
	      new_vertex->setCoord(rx,ry,rz);
	    }
		}
    else if(strcmp(line.c_str(),"<Lines>")==0)
		{
	  	for(i = 0;i < 4;i++)
	    	infile>>line;
		  new_li = new Edge();
		  new_mli = new Edge();
	  	edgef.push_back(new_li);
		  edgeb.push_back(new_mli);
		  for(i = 0;i < numl;i++)
	  	{
	      infile>>id1>>id2;
	      new_li = new Edge();
	      new_mli = new Edge();
	      new_li->vertex[0] = verts[id1];
	      new_li->vertex[1] = verts[id2];
	      new_li->opposite = new_mli;
	      new_mli->vertex[0] = verts[id2];
	      new_mli->vertex[1] = verts[id1];
	      new_mli->opposite = new_li;
	      new_li->lengthCheck();
	      new_mli->lengthCheck();
	      edgef.push_back(new_li);
	      edgeb.push_back(new_mli);

	      flag = false;
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
				{
				  if(verts[id1]->vertex[j]->id == id2)
		  		  flag = true;
				}
	      if(!flag)
				{
				  verts[id1]->vertex[verts[id1]->num_neighbors] = verts[id2];
				  verts[id1]->edge[verts[id1]->num_neighbors] = new_li;
				  verts[id1]->num_neighbors++;
				  verts[id2]->vertex[verts[id2]->num_neighbors] = verts[id1];
				  verts[id2]->edge[verts[id2]->num_neighbors] = new_mli;
				  verts[id2]->num_neighbors++;
				}
	    }
    }
    else if(strcmp(line.c_str(), "<Polys>")==0)
		{
		  for(i = 0;i < 4;i++)
	  	  infile>>line;
		  for(i = 0;i < numt;i++)
	    {
	      infile>>id1>>id2>>id3;
	      new_tri = new Face();
	      new_tri->vertex[0] = verts[id1];
	      new_tri->vertex[1] = verts[id2];
	      new_tri->vertex[2] = verts[id3];
	      for(j = 0;j < verts[id1]->num_neighbors;j++)
				{
		  		if(verts[id1]->vertex[j]->id == id2)
			    {
			      new_tri->edge[0] = verts[id1]->edge[j];
			      new_tri->edge[0]->face = new_tri;
		  	  }
				}
	      for(j = 0;j < verts[id2]->num_neighbors;j++)
				{
				  if(verts[id2]->vertex[j]->id == id3)
			    {
		  	    new_tri->edge[1] = verts[id2]->edge[j];
		    	  new_tri->edge[1]->face = new_tri;
			    }
				}
	      for(j = 0;j < verts[id3]->num_neighbors;j++)
				{
				  if(verts[id3]->vertex[j]->id == id1)
		    	{
			      new_tri->edge[2] = verts[id3]->edge[j];
			      new_tri->edge[2]->face = new_tri;
			    }
				}
	      faces.push_back(new_tri);
	    }
	  	done_flag = true;
		}
/*		else if(strcmp(line.c_str(), "<PointData")==0)
		{
			for(i = 0;i < 5;i++)
				infile>>line;
			for(i = 0;i < nump;i++)
			{
				infile>>phi;
				verts[i]->phi = 2.0 * phi - 1.0;
			}
			done_flag = true;
		}*/
  }
	
  infile.close();
  restartOrdering();
}


/* Ordering after restarting from file */
void restartOrdering()
{
  int i;
  int j;
  int k;
  int jp1;
  int num_neigh;
  int num_neigh_neigh;
  int ver_id;
  int next_ver_id;
  int correct_next_ver_id;
  int next_ver_loc;
  Vert *tmpvert;
  Edge *tmpedge;
    // Sorting of vertices - 02 September 2015
  for(i = 0;i < verts.size();i++)
  {
    num_neigh = verts[i]->num_neighbors;
    for(j = 0;j < num_neigh;j++)
		{
	  	ver_id = verts[i]->vertex[j]->id;
		  jp1 = (j + 1) % num_neigh;
		  next_ver_id = verts[i]->vertex[jp1]->id;
	  	for(k = 0;k < 3;k++)
	    {
	      if(verts[i]->edge[j]->face->vertex[k]->id != i && verts[i]->edge[j]->face->vertex[k]->id != ver_id)
				{
		  		correct_next_ver_id = verts[i]->edge[j]->face->vertex[k]->id;
				}
	    }
	  	if(next_ver_id != correct_next_ver_id)
	    {
	      for(k = 0;k < num_neigh;k++)
				{
		  		if(verts[i]->vertex[k]->id == correct_next_ver_id)
		    		next_ver_loc = k;
				}
	      tmpvert = verts[i]->vertex[jp1];
	      tmpedge = verts[i]->edge[jp1];
	      verts[i]->vertex[jp1] = verts[i]->vertex[next_ver_loc];
	      verts[i]->edge[jp1] = verts[i]->edge[next_ver_loc];
	      verts[i]->vertex[next_ver_loc] = tmpvert;
	      verts[i]->edge[next_ver_loc] = tmpedge;
	    }
		}
  }
}

void outputVTPFile(int t)
{
  unsigned int i;
  char outfile_name[1024];
  ofstream outfile;

  sprintf(outfile_name, "mem.%d.vtp", t);
  outfile.open(outfile_name, ios::out);
  outfile<<"<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">"<<endl;
  outfile<<"<PolyData>"<<endl;
  outfile<<"<Piece NumberOfPoints=\""<<verts.size()<<"\" NumberOfVerts=\"0\" NumberOfLines=\""<<edgef.size()-1<<"\" NumberOfStrips=\"0\" NumberOfPolys=\""<<faces.size()<<"\">"<<endl;

  outfile<<"<Points>"<<endl;
  outfile<<"<DataArray type=\"Float32\" Name=\"vertex_coordinates\" NumberOfComponents=\"3\" format=\"ascii\">"<<endl;
  for(i = 0;i < verts.size();i++)
  {
    outfile<<verts[i]->coord[0]<<" "<<verts[i]->coord[1]<<" "<<verts[i]->coord[2]<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"</Points>"<<endl;

  outfile<<"<Lines>"<<endl;
  outfile<<"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">"<<endl;
  for(i = 1;i < edgef.size();i++)
  {
    outfile<<edgef[i]->vertex[0]->id<<" "<<edgef[i]->vertex[1]->id<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">"<<endl;
  for(i = 1;i < edgef.size();i++)
  {
    outfile<<2*i<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"</Lines>"<<endl;

  outfile<<"<Polys>"<<endl;
  outfile<<"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">"<<endl;
  for(i = 0;i < faces.size();i++)
  {
    outfile<<faces[i]->vertex[0]->id<<" "<<faces[i]->vertex[1]->id<<" "<<faces[i]->vertex[2]->id<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">"<<endl;
  for(i = 0;i < faces.size();i++)
  {
    outfile<<(i+1)*3<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"</Polys>"<<endl;
/*	outfile<<"<PointData Scalars=\"Phi\">"<<endl;
  outfile<<"<DataArray type=\"Float32\" Name=\"Phi\" format=\"ascii\">"<<endl;
  for(i = 0;i < verts.size();i++)
  {
    outfile<<(verts[i]->phi+1.0)*0.5<<endl;
  }
  outfile<<"</DataArray>"<<endl;
  outfile<<"</PointData>"<<endl;
 */
  outfile<<"</Piece>"<<endl;
  outfile<<"</PolyData>"<<endl;
  outfile<<"</VTKFile>"<<endl;

  outfile.close();
}


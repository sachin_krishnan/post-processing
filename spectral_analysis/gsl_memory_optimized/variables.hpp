/*

Global variables in the simulation

*/

#ifndef VARIABLES_HPP
#define VARIABLES_HPP

#include <vector>
#include "data_structures.hpp"

extern vector <Vert *> verts;						// List of all vertices
extern vector <Face *> faces; 					// List of all Triangles
extern vector <Edge *> edgef; 						// List of all Links
extern vector <Edge *> edgeb; 						// List of all oppositely oriented links

extern Vert *new_vertex;	 					// Place holder for a new vertex
extern Face *new_tri; 							// Place holder for a new triangle
extern Edge *new_li; 								// Place holder for new link
extern Edge *new_mli;

#endif

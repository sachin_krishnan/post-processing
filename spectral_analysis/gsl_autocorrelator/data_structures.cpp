/*

Definition of the data structures

*/

#include "data_structures.hpp"
#include "extras.hpp"
#include <cstdio>
#include <cmath>

Vert::Vert() : id(0), num_neighbors(0), mean_curvature(0.0), area(0.0),
	       normal{0.0, 0.0, 0.0}, coord{0.0, 0.0,0.0},
	       vertex{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}, edge{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
{} // Clean initialization of Vertex object

Vert::~Vert()
{
  for(int i = 0;i < num_neighbors;i++)
    {
      this->vertex[i] = NULL;
      this->edge[i] = NULL;
    }
}

void Vert::setCoord(double a, double b, double c) 	// Set vertex coordinates in one line
{
  this->coord[0] = a;
  this->coord[1] = b;
  this->coord[2] = c;
}

void Vert::calculateArea()
{
  int i;
  this->area = 0.0;
  for(i = 0;i < this->num_neighbors;i++)
    {
      this->area += this->edge[i]->face->area;
    }
  this->area *= 0.333333333;
}

void Vert::calculateCurvature()
{
  int i;
  int im;
  int ip;
  // double lij_2;
  double v0[3];
  double v1[3];						
  double v2[3];						
  double v3[3];						
  double v4[3];						
  double v5[3];						
  double cot_theta[2];		
  double dot;
  double cross;
  double sigma;
  double fwei = 0.0;
  double sum[3] = {0.0, 0.0, 0.0};
  double nor[3] = {0.0, 0.0, 0.0};
  double aij;
  double nor_2;
	
  for(i = 0;i < this->num_neighbors;i++)
    {
      fwei += this->edge[i]->face->area;
      im = (i + this->num_neighbors - 1) % this->num_neighbors;
      ip = (i + 1) % this->num_neighbors;

      v1[0] = this->coord[0] - this->vertex[i]->coord[0];
      v1[1] = this->coord[1] - this->vertex[i]->coord[1];
      v1[2] = this->coord[2] - this->vertex[i]->coord[2];

      v2[0] = this->coord[0] - this->vertex[im]->coord[0];
      v2[1] = this->coord[1] - this->vertex[im]->coord[1];
      v2[2] = this->coord[2] - this->vertex[im]->coord[2];

      v3[0] = this->coord[0] - this->vertex[ip]->coord[0];
      v3[1] = this->coord[1] - this->vertex[ip]->coord[1];
      v3[2] = this->coord[2] - this->vertex[ip]->coord[2];

      v4[0] = v2[0] - v1[0];
      v4[1] = v2[1] - v1[1];
      v4[2] = v2[2] - v1[2];

      v5[0] = v3[0] - v1[0];
      v5[1] = v3[1] - v1[1];
      v5[2] = v3[2] - v1[2];

      // lij_2 = (v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);

      dot = v3[0] * v5[0] + v3[1] * v5[1] + v3[2] * v5[2];
      v0[0] = v3[1] * v5[2] - v3[2] * v5[1];
      v0[1] = v3[2] * v5[0] - v3[0] * v5[2];
      v0[2] = v3[0] * v5[1] - v3[1] * v5[0];
      cross = sqrt(v0[0] * v0[0] + v0[1] * v0[1] + v0[2] * v0[2]);
      cot_theta[0] = dot/cross;

      dot = v2[0] * v4[0] + v2[1] * v4[1] + v2[2] * v4[2];
      v0[0] = v2[1] * v4[2] - v2[2] * v4[1];
      v0[1] = v2[2] * v4[0] - v2[0] * v4[2];
      v0[2] = v2[0] * v4[1] - v2[1] * v4[0];
      cross = sqrt(v0[0] * v0[0] + v0[1] * v0[1] + v0[2] * v0[2]);
      cot_theta[1] = dot/cross;

      aij = (cot_theta[0] + cot_theta[1]);
      // bij = lij_2 * aij;
      // sigma += bij;
      sum[0] += (v1[0] * aij);
      sum[1] += (v1[1] * aij);
      sum[2] += (v1[2] * aij);
    }

  // sigma *= 0.25;
  fwei = 1.0 / fwei;
	
  for(i = 0;i < this->num_neighbors;i++)
    {
      nor[0] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[0];
      nor[1] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[1];
      nor[2] += (this->edge[i]->face->area*fwei)*this->edge[i]->face->normal[2];
    }

  nor_2 = sqrt(nor[0] * nor[0] + nor[1] * nor[1] + nor[2] * nor[2]);
  nor[0] /= nor_2;
  nor[1] /= nor_2;
  nor[2] /= nor_2;
  this->normal[0] = nor[0];
  this->normal[1] = nor[1];
  this->normal[2] = nor[2];
  sigma = 2.0 * this->area;
  // 0.5 from aij formula (optimization)
  // using sigma = 2.0 * A, because we need mean curvature here. The formula in Nelson, Piran, Weinberg calculates total curvature
  this->mean_curvature = 0.5 * (nor[0] * sum[0] + nor[1] * sum[1] + nor[2] * sum[2]) / sigma;
}

void Vert::removeNeigh(Vert *ve)
{
  int i, j;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve)
	{
	  for(j = i; j < this->num_neighbors - 1; j++)
	    {
	      this->vertex[j] = this->vertex[j+1];
	      this->edge[j] = this->edge[j+1];
	    }
	  i = this->num_neighbors;
	}
    }
  this->num_neighbors--;
  this->vertex[this->num_neighbors] = NULL;
  this->edge[this->num_neighbors] = NULL;
}

void Vert::replaceNeigh(Vert *ve1, Vert *ve2, Edge *lf)
{
  int i;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve1)
	{
	  this->vertex[i] = ve2;
	  this->edge[i] = lf;
	  i = this->num_neighbors;
	}
    }
}

void Vert::addNeighAfter(Vert *ve1, Vert *ve2, Edge *lf)
{
  int i;
  int j;
  for(i = 0;i < this->num_neighbors;i++)
    {
      if(this->vertex[i] == ve1)
	{
	  for(j = this->num_neighbors;j > i + 1;j--)
	    {
	      this->vertex[j] = this->vertex[j-1];
	      this->edge[j] = this->edge[j-1];
	    }
	  this->vertex[i + 1] = ve2;
	  this->edge[i + 1] = lf;
	  i = this->num_neighbors;
	}
    }
  this->num_neighbors++;
}

Face::Face() : area(0.0), volume(0.0), normal{0.0, 0.0, 0.0},
		       vertex{NULL, NULL, NULL}, edge{NULL, NULL, NULL}
{} // Clean initialization of Face object

Face::~Face()
{
  this->vertex[0] = NULL;
  this->vertex[1] = NULL;
  this->vertex[2] = NULL;
  this->edge[0] = NULL;
  this->edge[1] = NULL;
  this->edge[2] = NULL;
}

/* Calculate area, volume and normal for a triangle */
int Face::calculateArea()
{
  double ax;
  double ay;
  double az;
  double r1[3];
  double r2[3];
  double r3[3];
  double r21[3];
  double r31[3];
  
  for(int l = 0;l < 3;l++)
    {
      r1[l] = this->vertex[0]->coord[l];
      r2[l] = this->vertex[1]->coord[l];
      r3[l] = this->vertex[2]->coord[l];
    }

  r21[0] = r2[0] - r1[0];
  r21[1] = r2[1] - r1[1];
  r21[2] = r2[2] - r1[2];
  
  r31[0] = r3[0] - r1[0];
  r31[1] = r3[1] - r1[1];
  r31[2] = r3[2] - r1[2];
  
  ax = (r21[1] * r31[2] - r21[2] * r31[1]) * 0.5;
  ay = (r21[2] * r31[0] - r21[0] * r31[2]) * 0.5;
  az = (r21[0] * r31[1] - r21[1] * r31[0]) * 0.5;
  this->area = sqrt(ax*ax + ay*ay + az*az);
  if(this->area < 0.43)
    return 1;
  
  ax /= this->area;
  ay /= this->area;
  az /= this->area;
  
  this->normal[0] = ax;
  this->normal[1] = ay;
  this->normal[2] = az;
  
  this->volume = (r1[0] * (r2[1] * r3[2] - r2[2] * r3[1]) + r1[1] * (r2[2] * r3[0] - r2[0] * r3[2]) + r1[2] * (r2[0] * r3[1] - r2[1] * r3[0])) / 6.0;
  return 0;
}


Edge::Edge() : face(NULL), vertex{NULL, NULL}, opposite(NULL)
{} // Clean initialization of Edge object

Edge::~Edge()
{
  this->face = NULL;
  this->vertex[0] = NULL;
  this->vertex[1] = NULL;
  this->opposite = NULL;
}

int Edge::lengthCheck()
{
  double v21[3];
  
  v21[0] =  this->vertex[0]->coord[0] - this->vertex[1]->coord[0];
  v21[1] =  this->vertex[0]->coord[1] - this->vertex[1]->coord[1];
  v21[2] =  this->vertex[0]->coord[2] - this->vertex[1]->coord[2];
  double dis = v21[0] * v21[0] + v21[1] * v21[1] + v21[2] * v21[2];
  if(dis <= 1.0 || dis > 3.0)
    return 1;
  return 0;
}

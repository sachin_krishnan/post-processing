#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#define AUTO_CORRELATION 
#define CORR_STEPS 400

using namespace std;

int main(int argc, char *argv[])
{
	int i;
  int j;
  int k;
	int l;
	int lmax;	// LMAX
	int junk;	
  int num_st;

	// Taking input through command line arguments
  if(argc < 2)
  {	
    cout<<"Usage : <exec> <LMAX>"<<endl;
    return 0;
  }
	lmax = atoi(argv[1]);

  double progress;
	vector < double > ulm2avg;
	vector < double > ulm2var;
	vector < double > ulm2std;
	vector < vector < double > > ulm2val_0;
	vector < vector < double > > ulm2ijavg;
	vector < vector < double > > ulm2ijvar;
	vector < vector < double > > ulm2ijstd;
	vector < vector < ifstream * > > fulm2vals;
	ifstream *fulm2val;

	string sjunk;
  char infile[256];
  string temp;
  string line;
  ofstream fout;
  
	// Initializing all the data structures
	for(i = 0;i <= lmax;i++)
	{
		ulm2avg.push_back(0.0);
		ulm2std.push_back(0.0);
		ulm2var.push_back(0.0);
		ulm2val_0.push_back(vector < double > ());
		ulm2ijavg.push_back(vector < double > ());
		ulm2ijvar.push_back(vector < double > ());
		ulm2ijstd.push_back(vector < double > ());

		for(j = 0;j <= i;j++)
		{
			ulm2ijavg[i].push_back(0.0);
			ulm2ijvar[i].push_back(0.0);
			ulm2ijstd[i].push_back(0.0);
			ulm2val_0[i].push_back(0.0);
		}
	}
	
	// Open files for input
	for(j = 0;j <= lmax;j++)
	{
		fulm2vals.push_back(vector < ifstream * > ());
		for(k = 0;k <= j;k++)
		{
			sprintf(fname, "ulmvals_%d_%d.dat", j, k);
			fulm2val = new ifstream;

			(*fulm2val).open(fname, ios::in);
			fulm2vals[i]->push_back(fulm2val);
		}
	}

	int num_st = 0;
	int step;
	double val;
	// Calculates mean of |u_l^m|^2
	for(j = 0;j <= lmax;j++)
	{
	/*	for(k = 0;k < ulm2val[j].size();k++)
		{
			ulm2avg[j] += ulm2val[j][k];
		}
		ulm2avg[j] /= ulm2val[j].size();
*/
		for(k = 0;k <= j;k++)
		{

			while(!fulm2vals[j][k]->eof())
			{
				fulm2vals>>step>>val;
				ulm2ijavg[j][k] += val;
				num_st++;
			}
			num_st--;
			ulm2ijavg[j][k] /= num_st;
		}
	}
	
	// Calculates standard deviation and standard error in <|u_l^m|^2>
	for(j = 0;j <= lmax;j++)
	{
	/*	for(k = 0;k < ulm2val[j].size();k++)
		{
			ulm2var[j] += ((ulm2val[j][k] - ulm2avg[j]) * (ulm2val[j][k] - ulm2avg[j]));
		}
		ulm2var[j] /= ulm2val[j].size();
		ulm2std[j] = sqrt(ulm2var[j] / ulm2val[j].size());
		*/
		for(k = 0;k <= j;k++)
		{
			for(i = 0;i < ulm2ijval[j][k].size();i++)
			{
				ulm2ijvar[j][k] += ((ulm2ijval[j][k][i] - ulm2ijavg[j][k]) * (ulm2ijval[j][k][i] - ulm2ijavg[j][k]));
			}
			ulm2ijvar[j][k] /= ulm2ijval[j][k].size();
			ulm2ijstd[j][k] = sqrt(ulm2ijvar[j][k] / ulm2ijval[j][k].size());
		}
	}

/*	for(k = 0;k < ulm2ijval[2][2].size();k++)
	{
		cout<<ulm2ijval[2][2][k]<<endl;
	}
*/
	// Printing <|u_l|^2> output to file
	fout.open("ulm2avg.dat", ios::out);
	fout<<"# spherical harmonics analysis\n";
	for(j = 0;j <= lmax;j++)
	{
		fout<<j<<"\t"<<ulm2avg[j]<<"\t"<<ulm2std[j]<<"\t"<<ulm2var[j]<<endl;
	}
	fout.close();

	// Printing <|u_l^m|^2> output to files for each l to make figure 3
	char fname[256];
	for(j = 0;j <= lmax;j++)
	{
		strcpy(fname, "");
		sprintf(fname, "ulm2avg_%d.dat", j);
		fout.open(fname, ios::out);
		fout<<"# spherical harmonics analysis\n";
		for(k = 0; k <= j;k++)
		{
			fout<<k<<"\t"<<ulm2ijavg[j][k]<<"\t"<<ulm2ijstd[j][k]<<"\t"<<ulm2ijvar[j][k]<<endl;
		}
		fout.close();
	}
	
	// Autocorrelation data for figure 1
	#ifdef AUTO_CORRELATION
	double denom;
	for(j = 0;j <=lmax;j++)
	{
		for(k = 0;k <= j;k++)
		{
			for(i = 0;i < ulm2ijval[j][k].size()-CORR_STEPS;i++)
			{
				for(l = 0; l < CORR_STEPS;l++)
				{
					ulm2ijcor[j][k][l] += ((ulm2ijval[j][k][i] - ulm2ijavg[j][k]) * (ulm2ijval[j][k][i+l] - ulm2ijavg[j][k]));
				}
			}
		}
	}

	for(j = 0;j <= lmax;j++)
	{
		for(k = 0;k <= j;k++)
		{
			strcpy(fname, "");
			sprintf(fname, "ulmcorr_%d_%d.dat", j, k);
			fout.open(fname, ios::out);
			fout<<"# autocorrelation in spherical harmonics modes\n";
	
			for(l = 1; l < CORR_STEPS;l++)
			{
				ulm2ijcor[j][k][l] /= ulm2ijcor[j][k][0];	
			}
			ulm2ijcor[j][k][0] /= ulm2ijcor[j][k][0];

			for(l = 0; l < CORR_STEPS;l++)
			{
				fout<<l<<"\t"<<ulm2ijcor[j][k][l]<<endl;
			}
			fout.close();				
		}
	}

	#endif

	// Clear everything
  cout<<"\nDone."<<endl;
  r.clear();
	ulm_real.clear();
	ulm_imag.clear();
	ulm2std.clear();
	ulm2avg.clear();
	ulm2val.clear();
	ulm2ijval.clear();
	ulm2ijavg.clear();
	ulm2ijvar.clear();
	ulm2ijstd.clear();
	ulm2ijcor.clear();
	vheight.clear();
 	vtheta.clear();
	vphi.clear();
 	return 0;
}

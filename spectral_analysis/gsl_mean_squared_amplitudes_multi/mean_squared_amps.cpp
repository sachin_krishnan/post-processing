#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "extras.hpp"
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_sf_legendre.h>

using namespace std;

// We are re using the restart functions written in extras.cpp for the MC code here to read the output files and calculate per vertex properties. The variables below are the data structures needed to hold and process this information.
vector <Vert *> verts;
vector <Face *> faces;
vector <Edge *> edgef;
vector <Edge *> edgeb;
Vert *new_vertex;
Face *new_tri;
Edge *new_li;
Edge *new_mli;

int main(int argc, char *argv[])
{
	int f;
	int i;
  int j;
  int k;
	int l;
	int lmax;	// LMAX
  int in_time;	// Initial timestep (* in mem.*.vtp) Eg. 100
  int fi_time;	// Final timestep (* in mem.*.vtp)	Eg. 500 fi_time >= in_time
  int st_time;	// Steps between consecutive timesteps Eg. 10
  int cu_time;	// Stores the current timestep
  int nump;	// Number of vertices
	int numl;	// Number of edges
	int numt;	// Number of faces
	int junk;	
  int num_st;
  int OUT_FREQ;	// Output frequency in the MC code
	bool done_flag;
  double d;
  double sd;
  double x,y,z;
  double rx, ry, rz;
  double srx, sry, srz;
  double avg_h;
	gsl_sf_result result;

	// Taking input through command line arguments
  if(argc < 7)
  {	
    cout<<"Usage : <exec> <LMAX> <initial_time> <final_time> <step_time> <output_frequency> <data-folder(s)>"<<endl;
    return 0;
  }
	lmax = atoi(argv[1]);
  in_time = atoi(argv[2]);
  fi_time = atoi(argv[3]);
  st_time = atoi(argv[4]);
  OUT_FREQ = atoi(argv[5]);

  double progress;
  double vol;
  double omega;
	double total_area;
	vector < vector <double> > r;
  vector < double > vheight;
	vector < double > vtheta;
	vector < double > vphi;
	vector < vector < double > > ulm_real;
	vector < vector < double > > ulm_imag;
	vector < vector < double > > ulm2ijavg;
	vector < vector < vector < double > > > ulm2ijval; 
  vector < vector < ofstream* > > foutp;
	ofstream* ftmp;

	string sjunk;
  char infile[256];
  string temp;
  string line;
  ifstream fp;
  ofstream fout;
  stringstream iss;
  
	// Initializing all the data structures
	for(i = 0;i <= lmax;i++)
	{
		ulm_real.push_back(vector < double > ());
		ulm_imag.push_back(vector < double > ());
		ulm2ijavg.push_back(vector < double > ());
		ulm2ijval.push_back(vector < vector < double > >());
		foutp.push_back( vector < ofstream* > ());
		for(j = 0;j <= i;j++)
		{
			ulm_real[i].push_back(0.0);
			ulm_imag[i].push_back(0.0);
			ulm2ijavg[i].push_back(0.0);
			ulm2ijval[i].push_back(vector < double >());
		}
	}

	//Finding mean radius from mean volume by processing memprops.dat
	char fnames[256];
  k = 0;
	sd = 0.0;
	for(f = 6;f < argc;f++)
	{
		sprintf(fnames, "%s/memprops.dat", argv[f]);
		fp.open(fnames,ios::in);
  	while(getline(fp,line))   
  	{
    	stringstream iss(line);
    	if(fp.eof())
				break;
    	if(line.c_str()[0] != '#')
			{
		  	getline(iss, sjunk, ' ');
		  	cu_time = atoi(sjunk.c_str());
			  getline(iss, sjunk, ' ');
			  getline(iss, sjunk, ' ');
			 	getline(iss, sjunk, ' ');
				getline(iss, sjunk, ' ');
			 	vol = atof(sjunk.c_str());
			  cu_time /= OUT_FREQ;
			
			  if(cu_time >= in_time && cu_time <= fi_time && (cu_time - in_time)%st_time == 0 && vol!=0.0)
	    	{
		//			cout<<vol<<endl;
		      sd += vol;
	  	    k++;
	    	}
			}
  	}
		fp.close();
	}
  sd /= (double) k;
  sd = (sd*3.0)/(4.0*M_PI);
  sd = pow(sd, 1.0/3.0);
  
  cout<<"Mean radius = "<<sd<<endl;
 
 	num_st = 0;

	for(f = 6;f < argc;f++)
	{
  	cu_time = in_time;
 
 		// Processing the files one at a time
  	while(cu_time <= fi_time)
  	{
			// Prints the stylish progress bar
			cout<<"\rProcessing "<<argv[f]<<" "<<cu_time<<"...\t[";
	  	progress = (double) (cu_time - in_time) * 20.0 / (double) (fi_time - in_time);
	  	for(i = 1;i < 20;i++)
			{
			  if(i < progress)
			  	cout<<"#";
			  else
		  		cout<<" ";
			}	
		  cout<<"]";
	  	fflush(stdout);
	
			// Opens the configuration file and populates the data structures
			sprintf(infile, "%s/mem.%d.vtp", argv[f], cu_time);
			restartFromVTPFile(infile);
			nump = verts.size();
			numt = faces.size();
	
			// calculate per vertex area, to be used in solid angle calculation for spherical harmonics transform
			total_area = 0.0;
			for(i = 0;i < numt;i++)
			{
				faces[i]->calculateArea();
				total_area += faces[i]->area;
			}
			for(i = 0;i < nump;i++)
			{
				verts[i]->calculateArea();
			}
		
			// Find the Center of Mass of the vesical and translate it to origin
		  srx = 0.0;
    	sry = 0.0;
		  srz = 0.0;
			r.clear();
	  	for(i = 0;i < nump;i++)
			{
				r.push_back(vector <double>());
				rx = verts[i]->coord[0];
				ry = verts[i]->coord[1];
				rz = verts[i]->coord[2];
				r[i].push_back(rx);
   			r[i].push_back(ry);
	   		r[i].push_back(rz);
  	 		srx += rx;
   			sry += ry;
   			srz += rz;
	 		}
	       
  	  //Find COM
    	srx /= ((double) nump);
	    sry /= ((double) nump);
  	  srz /= ((double) nump);
    	// cout<<"COM coordinates = {"<<srx<<", "<<sry<<", "<<srz<<"} "<<endl;
				
			vheight.clear();
			vtheta.clear();
			vphi.clear();
	
	    for(i = 0;i < nump;i++)
			{
   			// Translating to COM coordinates
  			r[i][0] -= srx;
	   		r[i][1] -= sry;
  	 		r[i][2] -= srz;
    	
   			// Find radius
	   		d = sqrt((r[i][0] * r[i][0] + r[i][1] * r[i][1] + r[i][2] * r[i][2]));
  	 		vheight.push_back(d);		  
  		 	// Find latitude and longitude
	   		vtheta.push_back(acos(r[i][2] / d));	// Polar angle. 0 <= theta < PI
				vphi.push_back(atan2(r[i][1], r[i][0]) + M_PI);	// Azimuthal angle. 0 <= phi < 2PI 
	
	// 			cout<<i<<"\t"<<vheight[i]<<"\t"<<vtheta[i]*180.0/M_PI<<"\t"<<vphi[i]*180.0/M_PI<<endl;
			}
      
    	// Normalize with mean radius and get deviation of each vertex from mean radius
	    for(i = 0;i < nump;i++)
			{
   			vheight[i] /= sd;
   			vheight[i] -= 1.0;
	//			cout<<i<<"\t"<<vheight[i]<<endl;
 			}
   
			for(i = 0;i <= lmax;i++)
			{
				for(j = 0;j <= i;j++)
				{
					ulm_real[i][j] = 0.0;
					ulm_imag[i][j] = 0.0;
				}
			}

			// Performs the transformation to spherical harmonics coordinates
		 	for(i = 0;i < nump;i++)
			{
				//  This funcion calculates all normalized Legendre Polynomials for 0 \le l \le lmax and 0 \le m \le l for |x| \le 1.
				// int gsl_sf_legendre_array_e (const gsl_sf_legendre_t norm, const size_t lmax, const double x, const double csphase, double result_array[])
	//			gsl_sf_legendre_sphPlm_array(GSL_SF_LEGENDRE_SPHARM, lmax, cos(vtheta[i]), 1, result);
				omega = (verts[i]->area / total_area) * 4.0 * M_PI;
				for(j = 0;j <= lmax;j++)
				{
					for(k = 0;k <= j;k++)
					{		
						gsl_sf_legendre_sphPlm_e(j, k, cos(vtheta[i]), &result);
						ulm_real[j][k] += (omega * vheight[i] * result.val * cos(k * vphi[i]));
						ulm_imag[j][k] += (-1.0 * omega * vheight[i] * result.val * sin(k * vphi[i]));	
//					cout<<j<<"\t"<<k<<"\t"<<ulm_real[j][k]<<"\t"<<ulm_imag[j][k]<<endl;
					}	
				}
			}

			// Calculates |u_l^m|^2
			for(j = 0;j <= lmax;j++)
			{
				for(k = 0;k <= j;k++)
				{
					ulm2ijval[j][k].push_back(ulm_real[j][k]*ulm_real[j][k] + ulm_imag[j][k]*ulm_imag[j][k]);
				}
			}

		 	verts.clear();
			faces.clear();
			edgef.clear();
			edgeb.clear();
	  	cu_time += st_time;
	    num_st++;
  	}
		cout<<"\n";
	}
	// Calculates mean of |u_l^m|^2
//	num_st--;
	cout<<"\nNumber of configurations = "<<num_st<<endl;
	char fname[256];
	for(j = 0;j <= lmax;j++)
	{
		for(k = 0;k <= j;k++)
		{
			sprintf(fname, "mean_sq_amp_%d_%d.dat", j, k);
			ftmp = new ofstream;
			(*ftmp).open(fname, ios::out);
			foutp[j].push_back(ftmp);
		}
	}

	for(j = 0;j <= lmax;j++)
	{
		for(k = 0;k <= j;k++)
		{
			for(i = 0;i < ulm2ijval[j][k].size();i++)
			{
				ulm2ijavg[j][k] += ulm2ijval[j][k][i];
				(*foutp[j][k])<<i<<"\t"<<(ulm2ijavg[j][k] / (i+1))<<endl;
			}
		}
	}

	for(j = 0;j <= lmax;j++)
	{
		for(k = 0;k <= j;k++)
		{
			foutp[j][k]->close();
		}
	}

	// Clear everything
  cout<<"\nDone."<<endl;
	ulm_real.clear();
	ulm_imag.clear();
	ulm2ijval.clear();
	ulm2ijavg.clear();
	vheight.clear();
 	vtheta.clear();
	vphi.clear();
 	return 0;
}

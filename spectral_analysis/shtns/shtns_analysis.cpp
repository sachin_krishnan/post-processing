#include <iostream>
#include <fstream>
#include <sstream>
#include <fftw3.h>
#include <shtns.h>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <complex>
#include <vector>

#define EPSILON 1e-8
#define PI 3.141592653589793
#define RADTODEG 57.29577951308232

#define CROSS(dest,v1,v2)			\
  dest[0]=v1[1]*v2[2]-v1[2]*v2[1];		\
  dest[1]=v1[2]*v2[0]-v1[0]*v2[2];		\
  dest[2]=v1[0]*v2[1]-v1[1]*v2[0];

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define SUB(dest,v1,v2)				\
  dest[0]=v1[0]-v2[0];				\
  dest[1]=v1[1]-v2[1];				\
  dest[2]=v1[2]-v2[2];

using namespace std;

/* Algorithm for ray triangle intersection taken from "Fast, Minimum Storage Ray/Triangle Intersection" by Tomas Moller and Ben Trumbore */

int intersect_triangle(double orig[3], double dir[3], double vert0[3], double vert1[3], double vert2[3], double *t, double *u, double *v)
{
  double edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
  double det, inv_det;

  SUB(edge1,vert1,vert0);
  SUB(edge2,vert2,vert0);

  CROSS(pvec, dir, edge2);

  det = DOT(edge1, pvec);

#ifdef TEST_CULL
  if(det < EPSILON)
    return 0;

  SUB(tvec,orig,vert0);

  *u = DOT(tvec,pvec);

  if(*u < 0.0 || *u > det)
    return 0;

  CROSS(qvec,tvec,edge1);

  *v = DOT(dir,qvec);
  if(*v < 0.0 || *u + *v > det)
    return 0;

  *t = DOT(edge2,qvec);
  inv_det = 1.0/det;

  *t *= inv_det;
  *u *= inv_det;
  *v *= inv_det;

#else
  if(det > -EPSILON && det < EPSILON)
    return 0;
  inv_det = 1.0/det;

  SUB(tvec,orig,vert0);

  *u = DOT(tvec,pvec) * inv_det;
  if(*u < 0.0 || *u > 1.0)
    return 0;

  CROSS(qvec, tvec, edge1);

  *v = DOT(dir, qvec) * inv_det;
  if(*v < 0.0 || *u + *v > 1.0)
    return 0;

  *t = DOT(edge2, qvec) * inv_det;
#endif
  return 1;
}



int main(int argc, char *argv[])
{
  /* SHTNS variables */
  shtns_cfg shtns;                // handle to a sht transform configuration
  long int lmax,mmax,nlat,nphi,mres, NLM;

  cplx *Slm;      // spherical harmonics coefficients (l,m space): complex numbers.
  double *Sh;                // real space : theta,phi
  
  long int i,im,lm,j;
  long cu_time, in_time, fi_time, st_time;
  
  stringstream iss;
  double **r;
  int nump;
  double rx, ry, rz;
  double srx, sry, srz;
  double dvec[3];
  double ovec[3] = {0.0, 0.0, 0.0};

  vector < vector <int> > triangles;
	vector < vector < vector <double> > > ulm2;

  if(argc < 5)
  {
  	cout<<"exec <LMAX> <in_time> <fi_time> <st_time>\n";
	return 0;
  }
  in_time = atol(argv[2]);
  fi_time = atol(argv[3]);
  st_time = atol(argv[4]);

  lmax = atol(argv[1]);
  
	for(i = 0;i <= lmax;i++)
	{
		ulm2.push_back(vector < vector < double > >());
		for(j = 0;j <= mmax;j++)
		{
			ulm2[i].push_back(vector <double> ());
		}
	}
	nlat = 16;
  mmax = lmax;      nphi = 32;
  mres = 1;
  shtns_verbose(1);                       // displays informations during initialization.
  shtns_use_threads(0);           // enable multi-threaded transforms (if supported).
  //shtns = shtns_init( sht_reg_dct, lmax, mmax, mres, nlat, nphi );
  shtns = shtns_create(lmax, mmax, mres, sht_fourpi | SHT_REAL_NORM);
  shtns_set_grid(shtns, sht_reg_fast | SHT_PHI_CONTIGUOUS, 1e-14, nlat, nphi);
	NLM = shtns->nlm;
  cout<<"SHTNS NLM = "<<NLM<<endl;
	
  // Memory allocation : the use of fftw_malloc is required because we need proper 16-byte alignement.
  // allocate spatial fields.
  cout<<"Allocating "<<NSPAT_ALLOC(shtns)<<" for Sh"<<endl;
  Sh = (double *) fftw_malloc( NSPAT_ALLOC(shtns) * sizeof(double));
  // allocate SH representations.
  Slm = (cplx *) fftw_malloc( NLM * sizeof(cplx));
  
  LM_LOOP(shtns,  Slm[lm]=0.0; );
  /*for(i = 0;i <=lmax;i++)
  {
  	for(j = 0;j <= mmax && j <=lmax;j++)
	{
	  cout<<i<<"\t"<<j<<"\t"<<Slm[LM(shtns, i, j )]<<endl;
	}
  }*/

  /* Finding mean volume */
  ifstream infile;
  int k = 0;
  double sd;
  double vol;
  string line;
  string sjunk;
  string temp;
  infile.open("memprops.dat", ios::in);
  while(getline(infile, line))
  {
    stringstream iss(line);
    if(infile.eof())
      break;
    if(line.c_str()[0] != '#' && strlen(line.c_str()) > 10)
    { 
//			cout<<line<<endl;
      getline(iss, sjunk, ' ');
	  	cu_time = atol(sjunk.c_str());
		  getline(iss, sjunk, ' '); // Elastic energy
		  getline(iss, sjunk, ' '); // Total energy
		  getline(iss, sjunk, ' '); // Area	
		  getline(iss, sjunk, ' '); // Volume
		  vol = atof(sjunk.c_str());
//		cout<<cu_time<<"\t"<<vol<<endl;
		  cu_time /= 1000;
		  if(cu_time >= in_time && cu_time <= fi_time && (cu_time - in_time) % st_time == 0)
		  {
//		cout<<cu_time<<"\t"<<vol<<endl;
				sd += vol;
				k++;
	  	}
		}
  }
  sd /= (double) k;
  sd = (sd * 3.0) / (4.0 * M_PI);
  sd = pow(sd, 1.0 / 3.0);
  infile.close();

  cout<<"Mean radius considering "<<k<<" values = "<<sd<<endl;

	char fname[25];
  cu_time = in_time;

  while(cu_time <= fi_time)
  {
		sprintf(fname, "mem.%d.vtp", cu_time);	
  	infile.open(fname, ios::in); 
  	while(!infile.eof())
  	{
			infile>>temp;
			if(strcmp(temp.c_str(),"<Piece") == 0)
			{
			  infile>>temp;
			  iss.str(temp);
			  getline(iss,line,'\"');
	  		getline(iss,line,'\"');
			  nump = atoi(line.c_str());
			  r = (double **) malloc(sizeof(double *) * nump);
			  for(i = 0;i < nump;i++)
			  	r[i] = (double *) malloc(sizeof(double) * 3);
			}
  		else if(strcmp(temp.c_str(),"<Points>")==0)
	  	{
  			for(i = 0;i < 5;i++)
				  infile>>line;
	
				srx = 0.0;
				sry = 0.0;
				srz = 0.0;
				for(i = 0;i < nump;i++)
				{ 
				  infile>>rx>>ry>>rz;
			  	r[i][0] = rx;
				  r[i][1] = ry;
				  r[i][2] = rz;
				  srx += rx;
				  sry += ry;
				  srz += rz;
				}

				srx /= ((double) nump);
				sry /= ((double) nump);
				srz /= ((double) nump);

				cout<<"COM coords = {"<<srx<<", "<<sry<<", "<<srz<<"}\n";
				ovec[0] = srx;
				ovec[1] = sry;
				ovec[2] = srz;
			}
			else if(strcmp(temp.c_str(), "<Polys>")==0)
			{
	  		for(i = 0;i < 4;i++)
			    infile>>line;
			 	triangles.clear();
			 	for(i = 0;i < 2 * (nump - 2);i++)
			  {
			    int p, q, r;	
					infile>>p>>q>>r;
					triangles.push_back(vector<int>());
					triangles[i].push_back(p);
					triangles[i].push_back(q);
					triangles[i].push_back(r);
			  }
			  cout<<"Done reading triangle data. "<<triangles.size()<<endl;
			}
	  }
		infile.close();

	  int f = 0;
	  int isOn;
	  double t, u, v;
	  double ph, th;
// 		double mean_radius = 0.0;
		int ip, it;
  	for(it = 0;it < nlat;it++)
  	{
			th = acos(shtns->ct[it]);
    	for(ip = 0;ip < nphi;ip++)
  		{
// 	  		ph = ip * 2.0 * M_PI / nphi;
	//  		th = acos(shtns->ct[it]);
				ph= PHI_RAD(shtns, ip);	
	  		dvec[0] = sin(th)*cos(ph);
			  dvec[1] = sin(th)*sin(ph);
	  		dvec[2] = cos(th);
			  Sh[it * nphi + ip] = 0.0;
			  for(k = 0;k < triangles.size();k++)
			  {
					isOn = intersect_triangle(ovec, dvec, r[triangles[k][0]], r[triangles[k][1]], r[triangles[k][2]], &t, &u, &v);
					if(isOn && t > 0.0)
					{
	//	  			mean_radius += t;
	//					cout<<"Radius "<<t<<endl;	
						Sh[it * nphi + ip] = t;
//						cout<<it * nphi + ip<<endl;
		//  			f++;
//		  printf("%d\t%d\t%.10f\t%.10f\t%.10f\n", ip, it, ph, th, Sh[ip * nlat + it]);
		 				break;
					}
	  		}	
			}
  	}
//  mean_radius /= f;
//  cout<<"Mean Radius = "<<mean_radius<<endl;
  	for(it = 0;it < nlat;it++)
 		{ 
   		for(ip = 0;ip < nphi;ip++)
			{
	  		if(Sh[it * nphi + ip] != 0.0)
	  		{
			  	Sh[it * nphi + ip] /= sd;
	  			Sh[it * nphi + ip] -= 1.0;
		//	  	printf("%d\t%d\t%.10f\n",ip, it, Sh[ip * nlat + it]);
	  		}
			}
  	}
  	spat_to_SH(shtns, Sh, Slm);
	
		for(i = 0;i <= lmax;i++)
		{
			for(j = 0;j <= mmax && j <= i;j++)
			{
				ulm2[i][j].push_back(Slm[LM(shtns, i, j)].real() * Slm[LM(shtns, i, j)].real());
			}
		}
//  	for(i = 0;i < NLM;i++)
//    	if(Slm[i].real() < 1e-6)
//				Slm[i].real(0.0);

//  	for(i = 0; i <= lmax;i++)
//  	{
//    	for(j = 0;j <= mmax && j <= i;j++)
//			{
//	  		cout<<i<<"\t"<<j<<"\t"<<Slm[LM(shtns,i,j)].real()<<endl;
//			}
//  	}

  	for(i = 0;i < nump;i++)
  	{
			if(r[i] != NULL)
	  		free(r[i]);
 		}
  	if(r != NULL)
    	free(r);

		cu_time += st_time;
	}
	
	vector < vector <double> > ulm2_avg;
	for(i = 0;i <= lmax;i++)
	{
		ulm2_avg.push_back(vector <double> ());
		for(j = 0;j <= mmax && j<= i;j++)
		{
			ulm2_avg[i].push_back(0.0);
			for(k = 0;k < ulm2[i][j].size();k++)
			{
				ulm2_avg[i][j] += ulm2[i][j][k];
				if(i == 2)
				{
					cout<<k<<" "<<ulm2_avg[i][j] / (k+1.0)<<endl;
				}
			}
			ulm2_avg[i][j] /= ulm2[i][j].size();
	//		cout<<i<<" "<<j<<" "<<ulm2_avg[i][j]<<endl;
		}
	}

	if(Sh != NULL)
 		fftw_free(Sh);

/*
  ofstream outfile;
  outfile.open("recon.vtk", ios::out);
  outfile<<"# vtk DataFile Version 2.0\n";
  outfile<<"recon Output\n";
  outfile<<"ASCII\nDATASET POLYDATA\nPOINTS "<<nlat*nphi<<" float\n";
  double r, th, ph;
  int ip, it;
  for (it=0;it<nlat;it++) {
    for(ip=0;ip<nphi;ip++) {
      r = Th[ip*nlat+it];
      th = acos(shtns->ct[it]);
      ph = ip * 2.0 * M_PI / nphi;
      // cout<<th<<" \t "<<ph<<endl; 
      outfile<<(1.0 + r)*sin(th)*cos(ph)<<" "<<(1.0 + r)*sin(th)*sin(ph)<<" "<<(1.0 + r)*cos(th)<<"\n";
    }
  }
  outfile.close();
  */
	ulm2.clear();
  shtns_destroy(shtns);
}

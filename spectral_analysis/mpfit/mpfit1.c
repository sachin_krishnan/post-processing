#include "mpfit.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/* This is the private data structure which contains the data points
   and their uncertainties */
struct vars_struct {
  double *x;
  double *y;
  double *ey;
};

/*
 * linear fit function
 *
 * m - number of data points
 * n - number of parameters (2)
 * p - array of fit parameters
 * dy - array of residuals to be returned
 * vars - private data (struct vars_struct *)
 *
 * RETURNS: error code (0 = success)
 */
int ufunc(int m, int n, double *p, double *dy, double **dvec, void *vars)
{
  int i;
  struct vars_struct *v = (struct vars_struct *) vars;
  double *x, *y, *ey, f;

  x = v->x;
  y = v->y;
  ey = v->ey;

  for (i=0; i<m; i++) {
    f = 1.0 / (p[0] * (x[i] - 1.0) * (x[i] + 2.0) * (p[1] + x[i] * (x[i] + 1)));    
    dy[i] = (y[i] - f)/ ey[i];
   // f = 1.0 / (p[0] * (x[i] - 1.0) * (x[i] + 2) * (p[1] + x[i] * (x[i] + 1)));    
   // dy[i] = (y[i] - f) / ey[i];
    //f = - log(p[0]) - log((x[i]-1)*(x[i]+2))-log(p[1] + x[i] * (x[i] + 1));
    //dy[i] = (log(y[i]) - f);
  }

  return 0;
}


/* Test harness routine, which contains test data, invokes mpfit() */
int main(int argc, char *argv[])
{
  FILE *fp;
  /* X - independent variable */
  double *x;
  /* Y - measured value of dependent quantity */
  double *y;
  double *ey;   /* Measurement uncertainty - initialized below */
	double junk;
  double jk;
  double f;
  double p[2] = {10.0, 1.0};           /* Initial conditions */
  double perror[2];                   /* Returned parameter errors */
  int i;
  struct vars_struct v;  /* Private data structure */
  int status;
  double obj;
	int NUMLINES;
	char sjk[30];
  mp_result result;
  mp_config config;

	NUMLINES = atoi(argv[1]);
  fp = fopen(argv[2], "r");
  x = (double *) malloc(sizeof(double) * NUMLINES);
  y = (double *) malloc(sizeof(double) * NUMLINES);
  ey = (double *) malloc(sizeof(double) * NUMLINES);

  memset(&result,0,sizeof(result));       /* Zero results structure */
  memset(&config,0,sizeof(config));       /* Zero config structure */
  result.xerror = perror;
  config.maxiter = 200;

	fscanf(fp, "%s %s %s %s", sjk, sjk, sjk, sjk);
	fscanf(fp, "%lf %lf %lf %lf", &x[0], &y[0], &ey[0], &junk);
  fscanf(fp, "%lf %lf %lf %lf", &x[0], &y[0], &ey[0], &junk);
  for(i = 0;i < NUMLINES;i++)
  {
    fscanf(fp, "%lf %lf %lf %lf", &x[i], &y[i], &ey[i], &junk);
    printf("%lf %e %e\n",x[i],y[i],ey[i]);

  }
  fclose(fp);

  /* Fill private data structure */
  v.x = x;
  v.y = y;
  v.ey = ey;

  /* Call fitting function for 10 data points and 2 parameters */
  status = mpfit(ufunc, NUMLINES, 2, p, 0, &config, (void *) &v, &result);
  obj = 0.0;
  for (i=0; i<NUMLINES; i++) {
   f = 1.0 / (p[0] * (x[i] - 1.0) * (x[i] + 2) * (p[1] + x[i] * (x[i] + 1)));    
   obj += ((y[i] - f) * (y[i] - f))/ (ey[i] * ey[i]);
  //   f = 1.0 / (p[0] * (x[i] - 1.0) * (x[i] + 2) * (p[1] + x[i] * (x[i] + 1)));    
   //  obj += ((y[i] - f) * (y[i] - f) / (ey[i] * ey[i]));
    //    f = - log(p[0]) - log((x[i]-1)*(x[i]+2))-log(p[1] + x[i] * (x[i] + 1));
    //     obj += ((log(y[i]) - f) * (log(y[i]) - f));
  }
  printf("fit status = %d\n", status);
  switch(status)
  {
    case MP_OK_CHI:           /* Convergence in chi-square value */
      printf("Convergence in chi-square.\n");
    break;
    case MP_OK_PAR:           /* Convergence in parameter value */
      printf("Convergence in parameter.\n");
    break;
    case MP_OK_BOTH:          /* Both MP_OK_PAR and MP_OK_CHI hold */
      printf("Convergence in both chi-square and parameter.\n");
    break;
    case MP_OK_DIR:           /* Convergence in orthogonality */
      printf("Convergence in orthogonality.\n");
    break;
    case MP_MAXITER:          /* Maximum number of iterations reached */
      printf("Maximum number of iterations reached.\n");
    break;
    case MP_FTOL:             /* ftol is too small; no further improvement*/
      printf("ftol is too small; no further improvement.\n");
    break;
    case MP_XTOL:             /* xtol is too small; no further improvement*/
      printf("xtol is too small; no further improvement.\n");
    break;
    case MP_GTOL:             /* gtol is too small; no further improvement*/
      printf("gtol is too small; no further improvement.\n");
    break;

  }

  printf("Results | k = %e, s = %e \n", p[0], p[1]);
  printf("Errors  | k = %e, s = %e \n", perror[0], perror[1]);
  printf("chi_square = %e | %e\n", obj, result.bestnorm);
  return 0;
}

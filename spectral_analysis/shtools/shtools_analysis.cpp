#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#define NVERT 677

#define PI 3.141592653589793
#define RADTODEG 57.29577951308232

using namespace std;

extern "C" {void shexpandlsq_(double*, double*, double*, double*, int*, int*, int*, int*, int*); }
extern "C" {double makegridpoint_(double*, int*, double*, double*, int*, int*, int*); }

int main(int argc, char *argv[])
{
  int npoints = NVERT;
  int i;
  int j;
  int k;
  int count;
  int lmax;
  int in_time;
  int in_time_i;
  int in_time_f;
  int in_time_s;
  int fi_time;
  int st_time;
  int cu_time;
  int nump;
  int junk;
  int norm;
  int csphase;
  int num_st;
  bool done_flag;
  double sum_kappa;
  double sum_sigma;
  double var_kappa;
  double var_sigma;
  double d;
  double sd;
  double x,y,z;
  double lati, longi;
  double rx, ry, rz;
  double srx, sry, srz;
  double avg_h;
  double* h_data;		// height data
  double* lat;		// latitudes
  double* lon;		// longitudes
  double* cilm;
  double kappas[200];
  double sigmas[200];
  if(argc < 2)
    {	
      cout<<"Usage : <exec> <LMAX>"<<endl;
      return 0;
    }
  lmax = atoi(argv[1]);
  
  double progress;
  double vol;
  int chi2;
  vector < vector < vector < double > > > ylm;
  vector < double > mean_ulm2;
  vector < double > var_ulm2;
  vector < vector <double> > r;
  vector < vector <double> > du;
  vector < vector <double> > ulm2;
  string sjunk;
  
  char infile[256];
  string temp;
  string line;
  ifstream fp;
  ofstream fout;
  stringstream iss;
  st_time = 20;
  fi_time = 2000;
  sum_kappa = 0.0;
  sum_sigma = 0.0;
  count = 0;
  
  in_time_i = 100;
  in_time_f = 120;
  in_time_s = 2;
  
  for(in_time = in_time_i;in_time < in_time_f;in_time += in_time_s)
    {
      cilm = (double *) malloc(sizeof(double) * (lmax + 1) * (lmax + 1) * 2);
      h_data = (double *) malloc(sizeof(double) * npoints);
      lat = (double *) malloc(sizeof(double) * npoints);
      lon = (double *) malloc(sizeof(double) * npoints);
      for(i = 0;i < npoints;i++)
	{
	  r.push_back(vector <double> ());
	  for(j = 0;j < 3;j++)
	    {
	      r[i].push_back(0.0);
	      r[i].push_back(0.0);
	      r[i].push_back(0.0);
	    }
	}
      ylm.push_back(vector < vector < double > >());
      ylm.push_back(vector < vector < double > >());
      
      for(i = 0;i < lmax + 1;i++)
	{
	  mean_ulm2.push_back(0.0);
	  var_ulm2.push_back(0.0);
	  ylm[0].push_back(vector < double >());
	  ylm[1].push_back(vector < double >());
	  for(j = 0;j < lmax + 1;j++)
	    {
	      ylm[0][i].push_back(0.0);
	      ylm[1][i].push_back(0.0);
	    }
	  ulm2.push_back(vector <double> ());
	}
      
      //Finding mean volume
      fp.open("memprops.dat",ios::in);
      k = 0;
      sd = 0.0;
      while(getline(fp,line))   
	{
	  stringstream iss(line);
	  if(fp.eof())
	    break;
	  if(line.c_str()[0] != '#')
	    {
	      getline(iss, sjunk, ' ');
	      cu_time = atoi(sjunk.c_str());
	      getline(iss, sjunk, ' ');
	      getline(iss, sjunk, ' ');
	      vol = atof(sjunk.c_str());
	      cu_time /= 10000;

	      if(cu_time >= in_time && cu_time <= fi_time)
		{
		  sd += vol;
		  k++;
		}
	    }
	}
      sd /= (double) k;
      sd = (sd*3.0)/(4.0*PI);
      sd = pow(sd, 1.0/3.0);
      fp.close();
  
      // cout<<"Mean radius = "<<sd<<endl;
  
      cu_time = in_time;
      num_st = 0;
 
      while(cu_time <= fi_time)
	{
	  sprintf(infile, "mem.%d.vtp", cu_time);
	  fp.open(infile, ios::in);
	  done_flag = false;
	  while(!fp.eof() && !done_flag)
	    {
	      fp>>temp;
	      if(strcmp(temp.c_str(),"<Piece")==0)
		{
		  fp>>temp;
		  iss.str(temp);
		  getline(iss,line,'\"');
		  getline(iss,line,'\"');
		  nump = atoi(line.c_str());
		}
	      else if(strcmp(temp.c_str(),"<Points>")==0)
		{
		  // cout<<"\rProcessing "<<cu_time<<"...\t[";
		  //progress = (double) (cu_time - in_time) * 20.0 / (double) (fi_time - in_time);
		  //for(i = 1;i < 20;i++)
		  //  {
		  //    if(i < progress)
		  //	cout<<"#";
		  //    else
		  //	cout<<" ";
		  //  }
		  // cout<<"]";
		  //fflush(stdout);
	       
		  for(i = 0;i < 5;i++)
		    fp>>line;
	       
		  srx = 0.0;
		  sry = 0.0;
		  srz = 0.0;
		  for(i = 0;i < nump;i++)
		    {
		      fp>>rx>>ry>>rz;
		      r[i][0] = rx;
		      r[i][1] = ry;
		      r[i][2] = rz;
		      srx += rx;
		      sry += ry;
		      srz += rz;
		    }
	       
		  //Find COM
		  srx /= ((double) nump);
		  sry /= ((double) nump);
		  srz /= ((double) nump);
		  // cout<<"COM coordinates = {"<<srx<<", "<<sry<<", "<<srz<<"} "<<endl;
		  for(i = 0;i < nump;i++)
		    {
		      // Translating to COM coordinates
		      r[i][0] -= srx;
		      r[i][1] -= sry;
		      r[i][2] -= srz;
	      
		      // Find radius
		      d = sqrt((r[i][0] * r[i][0] + r[i][1] * r[i][1] + r[i][2] * r[i][2]));
		      h_data[i] = d;		   
		      //  sd += d;
		      // Trying a major change. 11 March 2015 16:25. lat[i] and lon[i] going to be calculated similar as in TestSHExpandLSQ
		   
		      lat[i] = atan2(r[i][2], sqrt(r[i][0] * r[i][0] + r[i][1] * r[i][1])) * 180.0 / PI;
		      lon[i] = atan2(r[i][1], r[i][0]) * 180.0 / PI;
		    }
	       
		  avg_h = 0.0;
		  for(i = 0;i < nump;i++)
		    {
		      h_data[i] /= sd;
		      h_data[i] -= 1.0;
		      avg_h += h_data[i];
		    }
		  //avg_h /= ((double) nump);
		  //for(i = 0;i < nump;i++)
		  //  {
		  //    h_data[i] -= avg_h;
		  //  }
	       
		  junk = 4; // choice of orthonormalization
		  csphase = 1; // CS phase factor 
		  shexpandlsq_(cilm, h_data, lat, lon, &npoints, &lmax, &junk, &chi2, &csphase);
		  done_flag = true;
		}
	    }	
	  fp.close();
		
	  for(i = 0;i < lmax + 1;i++)
	    {
	      for(j = 0;j < lmax + 1;j++)
		{
		  ylm[0][i][j] = cilm[j * (lmax + 1) * 2 + i * 2];
		  ylm[1][i][j] = cilm[j * (lmax + 1) * 2 + i * 2 + 1];
		}
	    }
	  for(i = 0;i < lmax + 1;i++)
	    {
	      ulm2[i].push_back(ylm[0][i][0] * ylm[0][i][0]);
	      for(j = 1;j <= i;j++)
		{
		  ulm2[i].push_back(ylm[0][i][j] * ylm[0][i][j]);
		  ulm2[i].push_back(ylm[1][i][j] * ylm[1][i][j]);
		}
	    }
	  cu_time += st_time;
	  num_st++;
	}

      for(i = 0;i < lmax + 1;i++)
	{
	  mean_ulm2[i] = 0.0;
	  for(j = 0;j < ulm2[i].size();j++)
	    {
	      mean_ulm2[i] += ulm2[i][j];
	    }
	  mean_ulm2[i] /= ((double) ulm2[i].size());

	  var_ulm2[i] = 0.0;
	  for(j = 0;j < ulm2[i].size();j++)
	    {
	      var_ulm2[i] += (ulm2[i][j] - mean_ulm2[i]) * (ulm2[i][j] - mean_ulm2[i]); 
	    }
	  var_ulm2[i] /= ((double) ulm2[i].size());
	}

      r.clear();
      ulm2.clear();
      if(lat != NULL)
	free(lat);
  
      if(lon != NULL)
	free(lon);
  
      if(h_data != NULL)
	free(h_data);
  
      if(cilm != NULL)
	free(cilm);

      cout<<mean_ulm2[1]<<"\t"<<mean_ulm2[2]<<"\t"<<mean_ulm2[3]<<"\t"<<mean_ulm2[2]/mean_ulm2[3]<<"\t";
      //if(mean_ulm2[2]/mean_ulm2[3] < 3.0)
      // 	continue;
      //      if(mean_ulm2[3] / mean_ulm2[1] < 5.0)
      //	 continue;
      kappas[count] = (1.0 / (60.0 * mean_ulm2[3])) - (1.0 / (24.0 * mean_ulm2[2]));
      //      sigmas[count] = (1.0 / (kappas[(in_time-in_time_i)/in_time_s])) * (1.0 / (2.0 * mean_ulm2[2]) - 1.0 / (10.0 * mean_ulm2[3]));
      sigmas[count] = (60.0 * mean_ulm2[3] - 12.0 * mean_ulm2[2]) / (2.0 * mean_ulm2[2] - 5.0 * mean_ulm2[3]);

      cout<<kappas[count]<<"\t"<<sigmas[count]<<endl;

      
      sum_kappa += kappas[count];
      sum_sigma += sigmas[count];
      count++;
    }
  sum_kappa /= ((double)(count));
  sum_sigma /= ((double)(count));

  var_kappa = 0.0;
  var_sigma = 0.0;
  for(i = 0;i < count;i++)
    {
      var_kappa += ((kappas[i] - sum_kappa)*(kappas[i] - sum_kappa));
      var_sigma += ((sigmas[i] - sum_sigma)*(sigmas[i] - sum_sigma));
   }
  var_kappa /= ((double)(count));
  var_sigma /= ((double)(count));
  var_kappa = sqrt(var_kappa);
  var_sigma = sqrt(var_sigma);

  cout<<"Kappa = "<<sum_kappa<<" +/- "<<var_kappa<<endl;
  cout<<"Sigma = "<<sum_sigma<<" +/- "<<var_sigma<<endl;
  
  
  return 0;
}

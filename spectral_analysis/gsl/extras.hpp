
#ifndef __EXTRAS_HPP__
#define __EXTRAS_HPP__

#include <cmath>
#include "data_structures.hpp"

void restartFromVTPFile(char *);
void restartOrdering();
void outputVTPFile(int);
#endif

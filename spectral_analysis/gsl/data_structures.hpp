/*

Definition of the data structures

*/

#ifndef DATA_STRUCTURES_HPP
#define DATA_STRUCTURES_HPP

using namespace std;

class Vert;
class Face;
class Edge;

class Vert 
{
public:
  Vert();
  ~Vert();
  
  int id; // Each vertex has a unique id
  int num_neighbors; // Number of neighbors (triangles or vertices)
	double mean_curvature; // Mean curvature of the vertex
  double area; // Area associated with the vertex
  double normal[3]; // Normal defined at the vertex
  double coord[3]; // Coordinates of the vertex 
  Vert *vertex[10]; // Pointers to neighboring vertices
  Edge *edge[10]; // Pointers to neighboring links
  
  void setCoord(double, double, double);
  void calculateArea();
  void calculateCurvature();
  void removeNeigh(Vert *);
  void replaceNeigh(Vert *, Vert *, Edge *);
  void addNeighAfter(Vert *, Vert *, Edge *);
};

class Face
{
public:
  Face();
  ~Face();

  double area; // Area of the triangle
  double volume; // Volume of the triangle
  double normal[3]; // Face normal
  Vert *vertex[3]; // Vertices making up the triangle
  Edge *edge[3]; // Links making up the triangle
  
  int calculateArea();
};

class Edge
{
public:
  Edge();
  ~Edge();
  
  Face *face; // Triangle associated with the link (by convention, the one on right side when you move along the link)
  Vert *vertex[2]; // End vertices of the link
  Edge *opposite; // The opposite link
  int lengthCheck();
};

#endif
